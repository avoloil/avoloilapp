// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiHost: 'https://new.avoloil.ru/server/public/api/',
  firebase: {
    apiKey: "AIzaSyB1XLimV_xPUMEaf7gGHHVoxTCqn5bL39M",
    authDomain: "avoloil.firebaseapp.com",
    databaseURL: "https://avoloil.firebaseio.com",
    projectId: "avoloil",
    storageBucket: "avoloil.appspot.com",
    messagingSenderId: "416854388134"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

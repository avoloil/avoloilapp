import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ProfileService } from './tabs/profile/shared/services/profile.service';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { Router } from '@angular/router';
import { UserUpdateService } from './shared/providers/user-update';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private navCtrl: NavController,
    private updateUser: UserUpdateService,
    private fcm: FCM,
    private storage: Storage,
    private profile: ProfileService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();

      this.fcm.subscribeToTopic('marketing');

      this.fcm.getToken().then(token => {
        // alert(token);
        console.log(token);
        localStorage['tokenDevice'] = token;
        // backend.registerToken(token);
      });

      this.fcm.onNotification().subscribe(data => {
        if (data.wasTapped) {
          this.navCtrl.navigateRoot(['app/tabs/dialog', data['chat_id']], { queryParams: { alians_id: data['user_id'] } });
        } else {
          this.profile.get(localStorage['userId']).subscribe(res => {
            this.updateUser.updateData(res['entity']);
          });
        }
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        // alert(token);
        console.log(token);
        localStorage['tokenDevice'] = token;
        // this.tokenService.sendPushNot({token}).subscribe();

        // backend.registerToken(token);
      });

      this.fcm.unsubscribeFromTopic('marketing');
    });
  }
}

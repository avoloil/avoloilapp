import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NavController, ModalController } from "@ionic/angular";
import { OilInfoService } from "./shared/oil-info.service";
import { ModalSlider } from "./shared/components/modal-slider.component";

@Component({
    selector: 'app-tabs-oil-info',
    templateUrl: './oil-info.component.html',
    styleUrls: ['./oil-info.component.scss']
})
export class OilInfoComponent {

    private entity: object = {};
    private info = null;

    constructor(public router: Router,
        private _oilInfo: OilInfoService,
        private modalController: ModalController,
        private navCtrl: NavController,
        public route: ActivatedRoute) { }

    ionViewWillEnter() {
        this.entity = this.route.snapshot.data['data']['entity'] || this.route.snapshot.data['data'][0]['entity'];
        if (this.route.snapshot.data['data'][1]) {
            this.info = this.route.snapshot.data['data'][1]['entity'];
        }
        console.log(this.info, this.entity);
    }

    shop() {
        this.navCtrl.navigateRoot(['/app/tabs/map'], { queryParams: { type: 'buy', oil_id: this.entity['oil_id'] } });
    }

    service() {
        this.navCtrl.navigateRoot(['/app/tabs/map'], { queryParams: { type: 'change', oil_id: this.entity['oil_id'] } });
    }

    addWish() {
        this._oilInfo.addWish(this.entity['id']).subscribe(res => {
            if (res['status']) {
                this.entity['favorite'] = 1;
            }
        });
    }

    minusWish() {
        this._oilInfo.minusWish(this.entity['id']).subscribe(res => {
            if (res['status']) {
                this.entity['favorite'] = 0;
            }
        });
    }

    openModal(arr: Array<object>, title: string) {
        this.presentModal(arr, title);
    }

    async presentModal(images: Array<object>, title: string) {
        const modal = await this.modalController.create({
            component: ModalSlider,
            componentProps: { images, title }
        });
        return await modal.present();

    }

    back() {
        // this.router.navigate(['/app/tabs/oil/list'], { queryParams: { index: this.route.parent.queryParams['_value']['index'] } });
        this.navCtrl.back();
    }
}
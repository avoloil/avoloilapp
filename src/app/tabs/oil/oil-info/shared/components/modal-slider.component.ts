import { Component, Input } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";

@Component({
    selector: 'modal-slider',
    templateUrl: './modal-slider.component.html',
    styleUrls: ['./modal-slider.component.scss']
})
export class ModalSlider {

    private images;
    private title;
    slidepts = {
        effect: 'flip'
    };

    constructor(private modalController: ModalController, private navParams: NavParams) {
        this.images = this.navParams.data.images;
        this.title = this.navParams.data.title;
    }

    dismiss() {
        this.modalController.dismiss();
    }

}
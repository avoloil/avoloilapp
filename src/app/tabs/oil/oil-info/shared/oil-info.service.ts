import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController, Platform } from "@ionic/angular";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { tap, toArray } from "rxjs/operators";
import { concat } from 'rxjs';

@Injectable()
export class OilInfoService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController, private platform: Platform) {
        super(request, toastController);
        this.service_name = 'oil-info';
    }

    resolve(route: ActivatedRouteSnapshot) {
        let id = route.params['id'];
        if (route.queryParams['info']) {
            const get = this.get(id);
            const info = this.getInfo(route.queryParams['info']);
            return new Promise((resolve, reject) => {
                concat(get, info)
                    .pipe(toArray())
                    .subscribe(x => {
                        resolve(x);
                    });
            });
        } else {
            return this.get(id);
        }
    }

    getInfo(id: number) {
        let url = this.url('info');
        url += '/' + id;
        return this.request.get(url);
    }

    addWish(oil_id: number) {
        let url = this.url('addWish');
        return this.request.post(url, { oil_id }).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Добавлено в список избранного');
                }
            })
        );
    }

    minusWish(oil_id: number) {
        let url = this.url('minusWish');
        return this.request.post(url, { oil_id }).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Удалено из списка избранного');
                }
            })
        );
    }




}
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {toArray} from 'rxjs/operators';
import {RequestService} from 'src/app/shared/services/request.service';
import {ToastController} from '@ionic/angular';
import {EntityService} from 'src/app/shared/base/entity.service';
import * as queryString from 'query-string';
import {concat, Observable} from 'rxjs';

@Injectable()
export class OilService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'oil';
    }

    resolve(route: ActivatedRouteSnapshot) {
        return this.getAllMarks();
    }

    getAllMarks() {
        const url = this.url('getMark');
        return this.request.get(url);
    }

    getAllYears(typeEngineId: number): Observable<any> | any {
        const url = this.url('getYears');
        return this.request.get(`${url}${typeEngineId}`);
    }

    getAllEngineType(markId: number): Observable<any> | any {
        const url = this.url('getEngineType');
        return this.request.get(`${url}${markId}`);
    }

    getAllFiter(yearId: number): Observable<any> | any {
        const url = this.url('getFilter');
        return this.request.get(`${url}${yearId}`);
    }

    getAllOilConsumption(yearId: number): Observable<any> | any {
        const url = this.url('getOilConsumption');
        return this.request.get(`${url}${yearId}`);
    }
}
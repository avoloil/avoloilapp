import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OilComponent } from "./oil.component";
import { OilListComponent } from "./oil-list/oil-list.component";
import { OilListService } from "./oil-list/shared/services/oil-list.service";
import { OilInfoComponent } from "./oil-info/oil-info.component";
import { OilInfoService } from "./oil-info/shared/oil-info.service";
import { OilCustomComponent } from "./oil-custom/oil-custom.component";
import { OilService } from "./shared/services/oil.service";
import { AuthGuard } from "src/app/shared/guard/auth-guard.service";

const routes: Routes = [
    {
        path: '',
        component: OilComponent,
        resolve: {
            data: OilService
        }
    },
    {
        path: 'list',
        children: [
            {
                path: '',
                component: OilListComponent,
                resolve: {
                    data: OilListService
                }
            },
            {
                path: 'info/:id',
                children: [
                    {
                        path: '',
                        component: OilInfoComponent,
                        resolve: {
                            data: OilInfoService
                        }
                    }
                ]
            },
        ],

    },
    {
        path: 'custom',
        canActivate: [AuthGuard],
        component: OilCustomComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OilRouterModule { }
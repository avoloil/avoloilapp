import { Component } from "@angular/core";
import { OilCustomService } from "./shared/services/oil-custom.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector: 'app-tabs-oil-custom',
    templateUrl: './oil-custom.component.html',
    styleUrls: ['./oil-custom.component.scss'],
})
export class OilCustomComponent {

    public entity: object = {};
    public checked: boolean = false;
    private check: boolean = false;

    constructor(
        public oilCustomService: OilCustomService,
        public router: Router) { }

    setForm() {
        this.oilCustomService.set(this.entity).subscribe(res => {
            this.entity = {};
            this.router.navigate(['/app/tabs/oil/success']);
        });
    }

    customSelect(ev: any) {
        this.checked = ev['detail']['checked'];
    }

    back() {
        this.router.navigate(['/app/tabs/oil']);
    }
}
import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { tap } from "rxjs/operators";

@Injectable()
export class OilCustomService extends EntityService {

    constructor(
        public request: RequestService,
        public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'oil-info';
    }

    set(data: object) {
        let url = this.url('index');
        return this.request.post(url, data).pipe(
            tap(res => {
                const msg = this.msg('index');
                this.presentToastWithOptions('success', msg, true);
            }, err => {
                this.presentToastWithOptions('danger', err.error.error);
            })
        );
    }
}
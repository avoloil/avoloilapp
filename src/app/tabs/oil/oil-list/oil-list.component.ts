import { Component, OnInit } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { OilListService } from "./shared/services/oil-list.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import * as queryString from 'query-string';
import {Events, NavController, Platform} from '@ionic/angular';
import { OilInfoService } from "../oil-info/shared/oil-info.service";

@Component({
    selector: 'app-user-oil-list',
    templateUrl: './oil-list.component.html',
    styleUrls: ['./oil-list.component.scss']
})
export class OilListComponent extends InfinitiScroll implements OnInit {

    public index: number;
    public filter: object;
    private status: boolean = false;

    constructor(
        public router: Router,
        private navCtrl: NavController,
        public route: ActivatedRoute,
        public oilListService: OilListService,
        private _oilInfo: OilInfoService,
        private platform: Platform,
        private event: Events,
        public storage: Storage) {
        super(oilListService);
    }

    ngOnInit() {
        // this.favorite = (this.platform.getQueryParam('favorite') == 'true');
        console.log(this.route.data);
        this.route.data.forEach(res => {
            if (res['data']['entity']) {
                this.toItems = res['data']['entity']['to'];
                this.list = res['data']['entity']['data'];
                this.list.map(el => el['favorite'] = true);
                this.params['page'] = res['data']['entity']['current_page'];
                this.totalItems = res['data']['entity']['total'];
            }
        });
        if (this.platform.getQueryParam('filter') != null) {
            this.is_filter = true;
            this.oil_id = `${this.route.snapshot.queryParams['condition']}${this.route.snapshot.queryParams['id']}`;
            this.filter = queryString.parse(this.platform.getQueryParam('filter'));
            this.route.data.subscribe((result: any) => {
                console.log(result);
                result.data.data.result.forEach(item => {
                    this.list.push(item.oil);
                });
            });
        } else {
            this.is_filter = false;
        }
        // if (!this.favorite) {
        this.params['category'] = this.route.snapshot.queryParams['id'];
        // }
    }

    back() {
        if (this.favorite) {
            this.router.navigate(['/app/tabs/profile'], {replaceUrl: true});
        } else {
            this.event.publish('replace', true);
            this.router.navigate(['/app/tabs'], {replaceUrl: true});
        }
    }

    changeParams() {
        this.router.navigate(['/app/tabs/oil']);
    }

    openInfo(id: number, info: number) {
        console.log(id);

        this.navCtrl.navigateRoot([`/app/tabs/oil/list/info`, id], { queryParams: { info } });
    }

    addWish(id: number, index: number) {
        this._oilInfo.addWish(id).subscribe(res => {
            if (res['status']) {
                this.list[index]['favorite'] = 1;
                this.list[index]['status'] = 1;
            }
        });
    }

    minusWish(id: number, index: number) {
        this._oilInfo.minusWish(id).subscribe(res => {
            if (res['status']) {
                this.list[index]['favorite'] = 0;
                this.list[index]['status'] = 0;
            }
        });
    }

}
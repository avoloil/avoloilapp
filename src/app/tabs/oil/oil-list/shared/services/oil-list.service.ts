import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController, Platform } from "@ionic/angular";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class OilListService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'oil-list';
    }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.queryParams['favorite'] == 'true') {
            return this.getAllFavorite();
        } else if (route.queryParams['filter'] != null) {
            return this.get(route.queryParams['condition'] + route.queryParams['id']);
        } else {
            let id = route.queryParams['id'];
            return this.get(null, { category: id });
        }
    }

    getAllFavorite() {
        let url = this.url('favorite');
        return this.request.get(url);
    }
}
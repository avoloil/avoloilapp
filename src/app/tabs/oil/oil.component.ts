import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {OilService} from './shared/services/oil.service';
import {Events, NavController} from '@ionic/angular';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import * as queryString from 'query-string';

@Component({
    selector: 'app-tabs-oil',
    templateUrl: './oil.component.html',
    styleUrls: ['./oil.component.scss']
})
export class OilComponent implements OnInit {

    mark: any = {
        header: 'Марка автомобиля'
    };
    engine: any = {
        header: 'Условие 1'
    };
    year: any = {
        header: 'Условие 2'
    };
    filter: any = {
        header: 'Условие 3'
    };
    oilConsumption: any = {
        header: 'Условие 4'
    };

    disableFormInput: any = {
        mark: true,
        condition1: true,
        condition2: true,
        condition3: true,
        condition4: true
    };

    disableSubmitButton: boolean = true;

    oilSelect: FormGroup;
    public marks: any = [];
    public condition1: any = [];
    public condition2: any = [];
    public condition3: any = [];
    public condition4: any = [];
    public lastCondition: string;
    public lastConditionId: number;
    public entity: object = {
        filter: true,
        index: null
    };

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private oilService: OilService,
        private navCtrl: NavController,
        private event: Events,
        fb: FormBuilder) {
        this.oilSelect = fb.group({
            mark_id: new FormControl(null, Validators.required),
            condition_id1: new FormControl(null),
            condition_id2: new FormControl(null),
            condition_id3: new FormControl(null),
            condition_id4: new FormControl(null),
            mark: new FormControl(null),
            condition1: new FormControl(null),
            condition2: new FormControl(null),
            condition3: new FormControl(null),
            condition4: new FormControl(null)
        });
    }

    ngOnInit() {
        this.marks = this.route.snapshot.data.data.data;
        this.event.subscribe('replace', (res: boolean) => {
            if (res) {
                this.clearData();
            }
        });
    }

    openCustom() {
        this.router.navigate(['/app/tabs/oil/custom']);
    }

    clearData(): void {
        this.oilSelect.patchValue({
            mark_id: null,
            condition_id1: null,
            condition_id2: null,
            condition_id3: null,
            condition_id4: null
        });
        this.disableFormInput = {
            condition1: true,
            condition2: true,
            condition3: true,
            condition4: true
        };
        this.disableSubmitButton = true;
    }

    changeMark(): void {
        this.oilSelect.patchValue({
            condition_id1: null,
            condition_id2: null,
            condition_id3: null,
            condition_id4: null
        });
        this.disableFormInput = {
            condition1: true,
            condition2: true,
            condition3: true,
            condition4: true
        };
        this.disableSubmitButton = true;
        if (this.oilSelect.value['mark_id'] != null) {
            this.oilService.getAllEngineType(this.oilSelect.value['mark_id']).subscribe((res: any) => {
                if (!res.data.result) {
                    this.condition1 = res.data.condition1;
                    this.disableFormInput.condition1 = false;
                } else this.disableSubmitButton = false;
            });
        }
    }

    changeCondition1(): void {
        this.oilSelect.patchValue({
            condition_id2: null,
            condition_id3: null,
            condition_id4: null
        });
        this.disableFormInput = {
            condition2: true,
            condition3: true,
            condition4: true
        };
        this.disableSubmitButton = true;
        if (this.oilSelect.value['condition_id1'] != null) {
            this.oilService.getAllYears(this.oilSelect.value['condition_id1']).subscribe((res: any) => {
                if (!res.data.result) {
                    this.condition2 = res.data.condition2;
                    this.disableFormInput.condition2 = false;
                } else {
                    this.disableSubmitButton = false;
                    this.lastConditionId = this.oilSelect.value.condition_id1;
                    this.lastCondition = 'condition1/';
                }
            });
        }
    }

    changeCondition2(): void {
        this.oilSelect.patchValue({
            condition_id3: null,
            condition_id4: null
        });
        this.disableFormInput = {
            condition3: true,
            condition4: true
        };
        this.disableSubmitButton = true;
        if (this.oilSelect.value['condition_id2'] != null) {
            this.oilService.getAllFiter(this.oilSelect.value['condition_id2']).subscribe((res: any) => {
                if (!res.data.result.length) {
                    this.condition3 = res.data.condition3;
                    this.disableFormInput.condition3 = false;
                } else {
                    this.disableSubmitButton = false;
                    this.lastConditionId = this.oilSelect.value.condition_id2;
                    this.lastCondition = 'condition2/';
                }
            });
        }
    }

    changeCondition3(): void {
        this.oilSelect.patchValue({
            condition_id4: null
        });
        this.disableFormInput = {
            condition4: true
        };
        this.disableSubmitButton = true;
        if (this.oilSelect.value['condition_id3'] != null) {
            this.oilService.getAllOilConsumption(this.oilSelect.value['condition_id3']).subscribe((res: any) => {
                if (!res.data.result.length) {
                    this.condition4 = res.data.condition4;
                    this.disableFormInput.condition4 = false;
                } else {
                    this.disableSubmitButton = false;
                    this.lastConditionId = this.oilSelect.value.condition_id3;
                    this.lastCondition = 'condition3/';
                }
            });
        }
    }

    changeCondition4(): void {
        if (this.oilSelect.value['condition_id4'] != null) {
            this.disableSubmitButton = false;
            this.lastConditionId = this.oilSelect.value.condition_id4;
            this.lastCondition = 'condition4/';
        }
    }

    openOil(id: number) {
        this.router.navigate(['/app/tabs/oil/list'], {queryParams: {id, favorite: 'false', filter: null}, replaceUrl: true});
    }

    openOilList() {
        const mark: string = this.marks.find(el => el['id'] == this.oilSelect.value['mark_id'])['title'];
        const condition1: string = this.oilSelect.value.condition_id1 ?
            this.condition1.find(el => el['id'] == this.oilSelect.value['condition_id1'])['title'] : '';
        const condition2: string = this.oilSelect.value.condition_id2 ?
            this.condition2.find(el => el['id'] == this.oilSelect.value['condition_id2'])['title'] : '';
        const condition3: string = this.oilSelect.value.condition_id3 ?
            this.condition3.find(el => el['id'] == this.oilSelect.value['condition_id3'])['title'] : '';
        const condition4: string = this.oilSelect.value.condition_id4 ?
            this.condition4.find(el => el['id'] == this.oilSelect.value['condition_id4'])['title'] : '';
        this.oilSelect.patchValue({mark, condition1, condition2, condition3, condition4});
        this.router.navigate(['/app/tabs/oil/list'], {
            queryParams: {
                filter: queryString.stringify(this.oilSelect.value),
                id: this.lastConditionId,
                favorite: 'false',
                condition: this.lastCondition
            }
        });
    }
}
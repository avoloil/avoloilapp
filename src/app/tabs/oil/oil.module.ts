import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "src/app/shared/shared.module";
import { OilRouterModule } from "./oil-router.module";
import { OilComponent } from "./oil.component";
import { OilListComponent } from "./oil-list/oil-list.component";
import { OilListService } from "./oil-list/shared/services/oil-list.service";
import { OilInfoComponent } from "./oil-info/oil-info.component";
import { OilInfoService } from "./oil-info/shared/oil-info.service";
import { OilCustomService } from "./oil-custom/shared/services/oil-custom.service";
import { OilCustomComponent } from "./oil-custom/oil-custom.component";
import { OilService } from "./shared/services/oil.service";
import { ModalSlider } from "./oil-info/shared/components/modal-slider.component";

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        OilRouterModule,
    ],
    declarations: [
        OilComponent,
        OilListComponent,
        OilInfoComponent,
        OilCustomComponent,
        ModalSlider
    ],
    providers: [
        OilListService,
        OilInfoService,
        OilCustomService,
        OilService
    ],
    entryComponents: [ModalSlider]
})
export class OilModule { }
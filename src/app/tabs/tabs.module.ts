import { DataNotService } from './../shared/providers/data-not-count.service';
import {IonicModule, IonTabs} from '@ionic/angular';
import { NgModule } from '@angular/core';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { SharedModule } from '../shared/shared.module';
import { ChatModule } from './comons/chat.module';
import { DialogModule } from './comons/dialog/dialog.module';
import { AuthGuard } from '../shared/guard/auth-guard.service';

@NgModule({
  imports: [
    IonicModule,
    SharedModule,
    TabsPageRoutingModule,
    ChatModule,
    DialogModule
  ],
  declarations: [TabsPage],
  providers: [AuthGuard]
})
export class TabsModule { }

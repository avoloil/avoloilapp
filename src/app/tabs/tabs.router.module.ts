import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { ChatListComponent } from './comons/chat-list/chat-list.component';
import { DialogComponent } from './comons/dialog/dialog.component';
import { ChatListService } from './comons/chat-list/shared/services/chat-list.service';
import { AuthGuard } from '../shared/guard/auth-guard.service';
import { ProfileService } from './profile/shared/services/profile.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tabs/oil',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    component: TabsPage,
    resolve: {
      data: ProfileService
    },
    children: [
      {
        path: '',
        redirectTo: 'oil',
        pathMatch: 'full'
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            canActivate: [AuthGuard],
            loadChildren: './home/home.module#HomeModule'
          }
        ]
      },
      {
        path: 'lib',
        children: [
          {
            path: '',
            loadChildren: './lib/lib.module#LibModule'
          }
        ]
      },
      {
        path: 'oil',
        children: [
          {
            path: '',
            loadChildren: './oil/oil.module#OilModule'
          }
        ]
      },
      {
        path: 'map',
        children: [
          {
            path: '',
            canActivate: [AuthGuard],
            loadChildren: './map/map.module#MapModule'
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            canActivate: [AuthGuard],
            loadChildren: './profile/profile.module#ProfileModule'
          }
        ]
      },
      {
        path: 'chat-list',
        component: ChatListComponent,
        // resolve: {
        //   data: ChatListService
        // }
      },
      {
        path: 'dialog/:id',
        component: DialogComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }

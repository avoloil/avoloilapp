import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { EntityService } from 'src/app/shared/base/entity.service';
import { RequestService } from 'src/app/shared/services/request.service';
import { Platform, ToastController } from '@ionic/angular';
import { tap } from 'rxjs/operators';

@Injectable()
export class MapService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService,
        public toastController: ToastController,
        private platform: Platform) {
        super(request, toastController);
        this.service_name = 'map';
    }

    resolve(route: ActivatedRouteSnapshot) {
        let obj = {};
        if (route.queryParams['type']) {
            obj['type'] = route.queryParams['type'];
        }
        if (route.queryParams['oil_id']) {
            obj['oil_id'] = route.queryParams['oil_id'];
        }
        obj['city'] = localStorage['city'];
        // type=shop&city=Минск&oil_id=3
        return this.get(null, obj);
    }

    addFavorite(id: number) {
        let url = this.url('add');
        return this.request.post(url, { partner_user_id: id })
            .pipe(tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Магазин добавлен в избранные');
                }
            }));
    }
}

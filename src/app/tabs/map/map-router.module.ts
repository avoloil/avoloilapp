import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MapComponent } from "./map.component";
import { MapService } from "./shared/services/map.service";
import { ShopInfoPage } from "./shop-info/shop-info.page";
import { ShopInfoService } from "./shop-info/shared/services/shop.service";

const routes: Routes = [
    {
        path: '',
        component: MapComponent,
        resolve: {
            data: MapService
        }
    },
    // {
    //     path: 'shop-info/:id',
    //     component: ShopInfoPage,
    //     resolve: {
    //         data: ShopInfoService
    //     }
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MapRouterModule { }
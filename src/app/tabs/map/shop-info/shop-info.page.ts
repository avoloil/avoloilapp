import { Input } from '@angular/core';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShopInfoService } from './shared/services/shop.service';
import { NavController, NavParams, ModalController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ViewController } from '@ionic/core';

@Component({
    selector: 'app-shop-info',
    templateUrl: './shop-info.page.html',
    styleUrls: ['./shop-info.page.scss'],
})
export class ShopInfoPage {

    @Input() shopId: number;
    private partner: object = {};

    constructor(private route: ActivatedRoute,
        private navCtrl: NavController,
        private callNumber: CallNumber,
        private modalController: ModalController,
        private navParams: NavParams,
        private _shopInfo: ShopInfoService) {
    }

    ionViewWillEnter() {
        console.log(this.navParams);
        this._shopInfo.get(this.shopId).subscribe(res => {
            this.partner = res['entity'];
        });
    }

    createChat() {
        this._shopInfo.createChat(this.partner['user_id']).subscribe(res => {
            console.log(res);
            if (res['status']) {
                this.navCtrl.navigateRoot(['/app/tabs/dialog', res['entity']['id']], { queryParams: { alians_id: this.partner['user_id'] } });
                this.modalController.dismiss();
            }
        });
    }

    toggleFavorite() {
        if (this.partner['favorite-partner']) {
            this.rmFavorite();
        } else {
            this.addFavorite();
        }
    }

    rmFavorite() {
        this._shopInfo.rmFavorite(this.partner['user_id']).subscribe(res => {
            if (res['status']) {
                this.partner['favorite-partner'] = 0;
            }
        });
    }

    addFavorite() {
        this._shopInfo.addFavorite(this.partner['user_id']).subscribe(res => {
            if (res['status']) {
                this.partner['favorite-partner'] = 1;
            }
        });
    }

    call(phone) {
        if (phone) {
            this.callNumber.callNumber(this.partner['additional_phone'], true)
                .then(res => console.log('Launched dialer!', res))
                .catch(err => console.log('Error launching dialer', err));
        }
    }

    back() {
        this.modalController.dismiss();
    }

}

import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { tap } from "rxjs/operators";

@Injectable()
export class ShopInfoService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'shop';
    }

    resolve(route: ActivatedRouteSnapshot) {
        return this.get(route.params['id']);
    }

    addFavorite(partnerId: number) {
        let url = this.url('addFavorite');
        return this.request.post(url, { partner_user_id: partnerId }).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Успешно добавлено в избранные');
                }
            })
        );
    }

    rmFavorite(userId: number) {
        let url = this.url('rmFavorite');
        return this.request.post(url, { partner_user_id: userId });
    }

    createChat(userId) {
        let url = this.url('createChat');
        return this.request.post(url, { user_id: userId });
    }
}
import { IonicModule } from '@ionic/angular';
import { NgModule } from "@angular/core";
import { ShopInfoPage } from './shop-info.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    imports: [IonicModule, SharedModule],
    declarations: [ShopInfoPage],
    exports: [ShopInfoPage],
    entryComponents: [ShopInfoPage]
})
export class ShopInfoModule { }
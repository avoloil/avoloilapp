import { ModalController } from '@ionic/angular';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MapService } from './shared/services/map.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { NavController } from '@ionic/angular';
import { ShopInfoPage } from './shop-info/shop-info.page';

declare const google: any;

@Component({
    selector: 'app-tabs-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent {

    @ViewChild('map') mapElement;
    map: any;
    private lat;
    private lng;
    private l;
    private g;
    private locations: Array<object> = [];
    pointMarkerImage = new Array;
    pointMarker = new Array;
    private list: Array<object> = [];
    private oil_id: boolean;
    constructor(
        private route: ActivatedRoute,
        private _map: MapService,
        private modalController: ModalController,
        private geolocation: Geolocation,
        private navCtrl: NavController) {
    }

    ionViewWillEnter() {
        this.route.data.forEach(res => {
            this.locations = [];
            res['data']['entity'].map(el => {
                // tslint:disable-next-line:max-line-length
                this.locations.push([`<div id="favorite${el.id}">${el.title}</div>`, el.lat, el.lng, el.id, el.role]);
            });
        });
        location.search.length ? this.oil_id = true : this.oil_id = false;
        this.l = localStorage['latitude'];
        this.g = localStorage['longitude']

        this.initMap();
    }

    initMap() {
        const that = this;
        var locations = this.locations;
        var service = this._map;
        var lat;
        var lng;
        var _navCtrl = this.navCtrl;
        setTimeout(() => {
            // if (locations.length) {
            //     var map = new google.maps.Map(document.getElementById('map'), {
            //         zoom: 12,
            //         center: new google.maps.LatLng(locations[0][1], locations[0][2]),
            //         mapTypeId: google.maps.MapTypeId.ROADMAP
            //     });
            // } else {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: new google.maps.LatLng(localStorage['latitude'], localStorage['longitude']),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            // }

            var infowindow = new google.maps.InfoWindow;

            var marker, i;
            let a = [];
            for (i = 0; i < locations.length; i++) {
                console.log(locations);
                let image;
                if (locations[i][4] == 'SHOP') {
                    image = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
                }
                if (locations[i][4] == 'SERVICE') {
                    image = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
                }
                if (locations[i][4] == 'DEALER') {
                    image = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: image,
                    dealerId: locations[i][3]
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                        console.log(locations[i][3]);
                        // this.addFavorite(locations[i][3]);
                        // setTimeout(() => {
                        a[`favorite` + locations[i][3]] = document.getElementById(`favorite${locations[i][3]}`);
                        that.openMap(locations[i][3]);
                        // _navCtrl.navigateRoot(['/app/tabs/map/shop-info', locations[i][3]]);
                        // }, 200);

                    };
                })(marker, i));
            }
        });
        // });
        console.log(locations);


    }
    async openMap(shopId) {
        const modal = await this.modalController.create({
            component: ShopInfoPage,
            // cssClass: 'fullsreenModal',
            componentProps: { shopId }
        });

        modal.present();
    }
    // public addFavorite(id) {
    //     console.log(id);

    // }

    segmentChanged(ev: any) {
        this._map.get(null, { city: localStorage['city'], type: ev['detail']['value'] }).subscribe(res => {
            this.locations = [];
            res['entity'].map(el => {
                this.locations.push([`<div id="favorite${el.id}">${el.title}</div>`, el.lat, el.lng, el.id, el.role]);
            });
            this.initMap();
        });
    }

    back() {
        window.history.back();
    }
}
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { MapRouterModule } from './map-router.module';
import { MapComponent } from './map.component';
import { MapService } from './shared/services/map.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ShopInfoPage } from './shop-info/shop-info.page';
import { ShopInfoService } from './shop-info/shared/services/shop.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ShopInfoModule } from './shop-info/shop-info.module';

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        MapRouterModule,
        ShopInfoModule
    ],
    declarations: [
        MapComponent,
    ],
    providers: [
        MapService,
        Geolocation,
        ShopInfoService,
        CallNumber
    ],
})
export class MapModule {
}
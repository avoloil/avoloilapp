import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { Resolve } from "@angular/router";

@Injectable()
export class HomeService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'home';
    }

    resolve() {
        return this.getNews();
    }

    getNews() {
        let url = this.url('index');
        return this.request.get(url);
    }
}
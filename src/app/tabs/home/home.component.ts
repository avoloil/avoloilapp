import { Component } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { HomeService } from "./shared/services/home.service";
import { ActivatedRoute } from "@angular/router";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

@Component({
    selector: 'app-tabs-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent extends InfinitiScroll {

    public slideOpts: object;

    constructor(
        public homeService: HomeService,
        public route: ActivatedRoute,
        private domSanitizer: DomSanitizer
    ) {
        super(homeService);
    }


    start(index: number) {
        console.log(['iframe' + index]);
        let iframe = document.getElementsByClassName('iframe' + index)[0];
        let start = document.getElementsByClassName('start' + index)[0];
        console.log(iframe);
        // let times = 0, playY;
        // let playVideo = iframe.addEventListener('click', () => {
        // if (times == 0) {
        // iframe.play();
        // times = 1;
        // }
        // })
        // let stop = document.getElementsByClassName('stop' + index)[0];
        // stop.addEventListener('click', () => {
        //     playY = playY.slice(0, -11);
        //     iframe['src'] = playY;
        //     times = 0;
        // });
    }

    stop(index: number) {

    }

    ionViewWillEnter() {
        this.route.data.forEach(res => {
            this.toItems = res['data']['entity']['to'];
            this.list = res['data']['entity']['data'];
            this.list.map(el => {
                if (el['url'] != null && typeof el['url'] === 'string') {
                    console.log(el['url'].includes('watch'));
                    if (el['url'].includes('watch')) {
                        el['url'] as SafeResourceUrl;
                        el['url'] = el['url'].replace('watch?v=', 'embed/');
                    }
                    el['url'] = this.domSanitizer.bypassSecurityTrustResourceUrl(el['url']);
                }
            });
            this.params['page'] = res['data']['entity']['current_page'];
            this.totalItems = res['data']['entity']['total'];
        }); this.slideOpts = {
            effect: 'flip',
            autoHeight: true
        };
    }

    doRefresh(event) {
        setTimeout(() => {
            this.list = [];
            this.params['page'] = 1;
            this.getAll();
            event.target.complete();
        }, 1000);
    }

    loadData(event) {
        setTimeout(() => {
            this.params['page']++;
            if (this.toItems == this.totalItems) {
                this.params['page']--;
            } else {
                this.getAll();
            }
            event.target.complete();
        }, 500);
    }

    getAll() {
        if (this.pageUrl) {
            this.id = localStorage['partnerId'] + '/getCanisters';
        }
        if (this.is_filter) {
            this.id = 'getOil/' + this.oil_id;
        }
        this.service.get(this.id, this.params)
            .subscribe(success => {
                this.list = this.list.concat(success['entity']['data']);
                this.list.map(el => {
                    if (el['url'] != null && typeof el['url'] === 'string') {
                        console.log(el['url'].includes('watch'));
                        if (el['url'].includes('watch')) {
                            el['url'] as SafeResourceUrl;
                            el['url'] = el['url'].replace('watch?v=', 'embed/');
                        }
                        el['url'] = this.domSanitizer.bypassSecurityTrustResourceUrl(el['url']);
                    }
                });
                this.params['page'] = success['entity']['current_page'];
                this.totalItems = success['entity']['total'];
                this.toItems = success['entity']['to'];
            });
    }
}
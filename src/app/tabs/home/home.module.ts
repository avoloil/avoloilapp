import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "src/app/shared/shared.module";
import { HomeRouterModule } from "./home-router.module";
import { HomeComponent } from "./home.component";
import { HomeService } from "./shared/services/home.service";

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        HomeRouterModule,
    ],
    declarations: [
        HomeComponent
    ],
    providers: [HomeService]
})
export class HomeModule { }
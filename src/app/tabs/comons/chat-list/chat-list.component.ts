import { ProfileService } from './../../profile/shared/services/profile.service';
import { Storage } from '@ionic/storage';
import { Component } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { ChatListService } from "./shared/services/chat-list.service";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireDatabase } from "angularfire2/database";
import { ShopInfoService } from '../../map/shop-info/shared/services/shop.service';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-tabs-chat-list',
    templateUrl: './chat-list.component.html',
    styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent extends InfinitiScroll {

    private messagesCollection: Array<any> = [];
    private chatCollection: any;
    private unreadList: any;

    public count: number = 0;
    private user: any;

    constructor(
        private db: AngularFireDatabase,
        public router: Router,
        public route: ActivatedRoute,
        private navCtrl: NavController,
        private profileService: ProfileService,
        private chatCreate: ShopInfoService,
        private storage: Storage,
        public chatListService: ChatListService) {
        super(chatListService);
    }

    ionViewWillEnter() {
        this.count = 0;
        this.profileService.get(localStorage['userId']).subscribe(res => {
            this.user = res['entity'];
            this.chatListService.get().subscribe(chat => {
                this.toItems = chat['entity']['to'];
                this.list = chat['entity']['data'];
                this.params['page'] = chat['entity']['current_page'];
                this.totalItems = chat['entity']['total'];
                this.view = true;
            });
        });
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }

    openDialog(id: number, alians_id: number) {
        console.log(alians_id);
        this.router.navigate(['/app/tabs/dialog', id], { queryParams: { alians_id } });
    }

    openDialogAdmin(id: number, alians_id: number) {
        this.chatCreate.createChat(alians_id).subscribe(res => {
            if (res['status']) {
                this.navCtrl.navigateRoot(['/app/tabs/dialog', res['entity']['id']], { queryParams: { alians_id } });
            }
        });
    }

    getAll() {
        if (this.pageUrl) {
            this.id = localStorage['partnerId'] + '/getCanisters';
        }
        if (this.is_filter) {
            this.id = 'getOil/' + this.oil_id;
        }
        this.service.get(this.id, this.params)
            .subscribe(success => {
                this.list = this.list.concat(success['entity']['data']);
                this.params['page'] = success['entity']['current_page'];
                this.totalItems = success['entity']['total'];
                this.toItems = success['entity']['to'];
            });
    }

    loadData(event) {
        setTimeout(() => {
            this.params['page']++;
            if (this.toItems == this.totalItems) {
                this.params['page']--;
            } else {
                this.getAll();
            }
            event.target.complete();
        }, 500);
    }

    doRefresh(event) {
        this.view = false;
        setTimeout(() => {
            this.list = [];
            this.count = 0;
            this.params['page'] = 1;
            this.getAll();
            event.target.complete();
        }, 1000);
    }
}
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "src/app/shared/shared.module";
import { MomentModule } from "angular2-moment";
import { FirebaseModule } from "src/app/shared/firebase.module";
import { DialogService } from "./shared/services/dialog.service";
import { DialogComponent } from "./dialog.component";
import { TextareaDialogDirective } from "./shared/directives/textarea-dialog.directive";
import { ShopInfoService } from "../../map/shop-info/shared/services/shop.service";

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        MomentModule,
        FirebaseModule],
    declarations: [
        DialogComponent,
        TextareaDialogDirective
    ],
    providers: [
        DialogService,
        ShopInfoService
    ]
})
export class DialogModule {
}

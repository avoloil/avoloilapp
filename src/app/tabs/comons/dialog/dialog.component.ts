import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { DialogService } from './shared/services/dialog.service';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { keyframes } from '@angular/animations';
import { NavController } from '@ionic/angular';
import { ProfileService } from '../../profile/shared/services/profile.service';

@Component({
    selector: 'page-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {

    @ViewChild('content') private content: any;

    private room_id: number;
    private alians_id: number;
    public user = {};
    private member_id: number;
    public msg: string;
    private unObj = {};

    private messagesCollection: any;
    private chatCollection: AngularFireList<any>;
    // items: Observable<any[]>;
    items
    private unreadList: any;
    unreadObj: Observable<any[]>;
    private active = false;
    constructor(public router: Router,
        public route: ActivatedRoute,
        private navCtrl: NavController,
        private db: AngularFireDatabase,
        public dialogService: DialogService,
        private profileService: ProfileService,
        public storage: Storage) {
        this.alians_id = +this.route.queryParams['_value']['alians_id'];
        this.room_id = this.route.params['_value']['id'];
        this.chatCollection = this.db.list('chats/' + this.room_id + '/messages');
        this.unreadList = this.db.object('chats/' + this.room_id + '/unread');
        this.items = this.chatCollection.snapshotChanges().pipe(
            map(changes => changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))));

        setTimeout(() => {
            this.content.scrollToBottom();
        });
    }

    ionViewWillEnter() {
        this.profileService.get(localStorage['userId']).subscribe(res => {
            this.user = res['entity'];
            this.unObj = {};
            this.unObj[this.user['id']] = 0;
            this.unObj['room_id'] = this.room_id;
            this.unreadList.update(this.unObj);
            this.items.subscribe(res => {
                if (res.length && this.active) {
                    if (this.user['id'] != res[res.length - 1]['chat_user_id'] && res.length) {
                        this.chatCollection.update(res[res.length - 1]['key'], { unread: false });
                    }
                }
            });
            this.active = true;
            setTimeout(() => {
                this.unreadList.snapshotChanges().subscribe(action => {
                    this.unObj = action.payload.val();
                    if (this.active) {
                        this.unObj[localStorage['userId']] = 0;
                        this.unreadList.update(this.unObj);
                    }
                });
            });
        });
    }

    ionViewDidLeave() {
        this.active = false;
    }

    sendMsg(text: string = '') {
        if (this.alians_id == this.user['id']) {
            this.dialogService.chatLogout();
        } else {
            if (text.trim().length) {
                let msg = {
                    chat_user_id: this.user['id'],
                    date: new Date().toString(),
                    text,
                    unread: true,
                    role: this.user['role'],
                    room_id: this.room_id
                }
                if (this.user['role'] != 'USER' && this.user['role'] != 'ADMIN') {
                    msg['title'] = this.user['partner']['title'];
                }
                if (!isNaN(this.unObj[this.alians_id])) {
                    this.unObj[this.alians_id]++;
                } else {
                    this.unObj[this.alians_id] = 1;
                }
                this.chatCollection.push(msg);
                this.unreadList.update(this.unObj);
                this.dialogService.sendPushNot(this.alians_id).subscribe();
                setTimeout(() => {
                    this.content.scrollToBottom();
                });
                this.msg = '';
            }
        }
    }

    back() {
        this.navCtrl.navigateRoot(['/app/tabs/chat-list']);

    }

}

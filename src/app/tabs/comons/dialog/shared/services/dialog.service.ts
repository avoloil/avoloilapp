import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { Router } from "@angular/router";

@Injectable()
export class DialogService extends EntityService {

    constructor(public request: RequestService, public toastController: ToastController, public router: Router) {
        super(request, toastController);
        this.service_name = 'chat';
    }

    sendPushNot(user_id: number) {
        let url = this.url('push');
        url += '/' + user_id;
        return this.request.post(url, {});
    }

    chatLogout() {
        localStorage.clear();
        this.presentToastWithOptions('success', 'Произошла ошибка, ваши данные были изменены, перезайдите в приложение');
        this.router.navigate(['/auth']);
    }
}
import { DirectiveModule } from './../../shared/directives/directives.module';
import { NgModule } from "@angular/core";
import { SharedModule } from "src/app/shared/shared.module";
import { ChatListComponent } from "./chat-list/chat-list.component";
import { ChatListService } from "./chat-list/shared/services/chat-list.service";
import { MomentModule } from "angular2-moment";
import { IonicModule } from "@ionic/angular";
import { FirebaseModule } from "src/app/shared/firebase.module";
import { ShopInfoService } from "../map/shop-info/shared/services/shop.service";

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        MomentModule,
        FirebaseModule,
        DirectiveModule
    ],
    declarations: [
        ChatListComponent,
    ],
    providers: [
        ChatListService,
        ShopInfoService
    ],
    // exports: [ChatListComponent]
})
export class ChatModule {

}
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AssortsService } from "./shared/services/assorts.service";
import { Storage } from "@ionic/storage";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";

@Component({
    selector: 'app-tabs-assorts',
    templateUrl: './assorts.component.html',
    styleUrls: ['./assorts.component.scss']
})
export class AssortsComponent extends InfinitiScroll {

    private user: object;

    constructor(
        private route: ActivatedRoute,
        public router: Router,
        public _assortService: AssortsService,
        private storage: Storage) {
        super(_assortService);
        this.assort = true;
    }

    ionViewWillEnter() {
        this.canisters = [];
        this._assortService.get().subscribe(res => {
            this.list = res['entity']['data'];
            this.list.map(el => {
                if (el['in_assortment']) {
                    this.canisters.push(el['id']);
                }
            });
            this.toItems = res['entity']['to'];
            this.params['page'] = res['entity']['current_page'];
            this.totalItems = res['entity']['total'];
        });
    }

    createSale() {
        this.router.navigate(['/app/tabs/profile/add-sale']);
    }

    saveAssort() {
        this._assortService.save({ canisters: this.canisters }).subscribe();
    }

    addAssort(id: number) {
        let x = this.canisters.indexOf(id);
        if (x === -1) {
            this.canisters.push(id);
        } else {
            this.canisters.splice(x, 1);
        }
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }

}
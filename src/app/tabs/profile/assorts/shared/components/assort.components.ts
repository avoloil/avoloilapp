// import { Injectable } from "@angular/core";
// import { AssortsService } from "../services/assorts.service";
// import { CanisterService } from "../services/canisters.service";

// @Injectable()
// export class AssortScroll {

//     public list: any[] = [];
//     public id: number;
//     public canisters: Array<number> = [];
//     public totalItems: number;
//     public currentPage: number;
//     public itemsPerPage: number;
//     public toItems: number;
//     public params: object = {};
//     constructor(
//         public _assortService: AssortsService,
//         public _canisterService: CanisterService) {
//         this.currentPage = 1;
//         this.itemsPerPage = 10;
//         this.params = { page: this.currentPage, limit: this.itemsPerPage };
//     }

//     getAll() {
//         let id = localStorage['partnerId'] + '/getCanisters';
//         this._canisterService.get(null, this.params)
//             .subscribe(success => {
//                 this.canisters = [];
//                 this.list = this.list.concat(success['entity']['data']);
//                 this.currentPage = success['entity']['current_page'];
//                 this.totalItems = success['entity']['total'];
//                 this.toItems = success['entity']['to'];
//                 this._assortService.get(id, this.params)
//                     .subscribe(res => {
//                         this.list.map((el, index) => {
//                             res['entity']['data'].map(item => {
//                                 let x = this.canisters.indexOf(item.id);
//                                 if (x === -1) {
//                                     this.canisters.push(item.id);
//                                 }
//                                 if (el.id === item.id) {
//                                     el['checked'] = true;
//                                 }
//                             });
//                         });
//                     });
//             });
//     }

//     loadData(event) {
//         setTimeout(() => {
//             this.currentPage++;
//             if (this.toItems == this.totalItems) {
//                 this.currentPage--;
//             } else {
//                 this.getAll();
//             }
//             event.target.complete();
//         }, 500);
//     }

//     doRefresh(event) {
//         setTimeout(() => {
//             this.list = [];
//             this.currentPage = 1;
//             this.getAll();
//             event.target.complete();
//         }, 1000);
//     }
// }
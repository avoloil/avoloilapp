import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { Resolve } from "@angular/router";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { concat } from "rxjs";
import { toArray, tap } from "rxjs/operators";

@Injectable()
export class AssortsService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'assorts';
    }

    resolve() {
        return this.get();
    }

    save(data: object) {
        let url = this.url('save');
        url += '/' + localStorage['partnerId'] + '/addCanister';
        return this.request.post(url, data).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Сохранено');
                }
            })
        );
    }

    sale(data: object) {
        if (data['user_id'] != null) {
            let url = this.url('sale');
            return this.request.post(url, data)
                .pipe(tap(res => {
                    if (res['status']) {
                        this.presentToastWithOptions('success', 'Продано');
                    }
                }));
        } else {
            this.presentToastWithOptions('danger', 'Поле ID покупателя обязательное');
        }
    }
}
import { Component } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { Router, ActivatedRoute } from "@angular/router";
import { SaleService } from "./shared/services/sale.service";
import { Storage } from "@ionic/storage";
import { NavController } from "@ionic/angular";

@Component({
    selector: 'app-tabs-sale',
    templateUrl: './sale.component.html',
    styleUrls: ['./sale.component.scss']
})
export class SaleComponent extends InfinitiScroll {

    // public tableHeader: Array<string> = [];
    private user: object;

    constructor(
        private route: ActivatedRoute,
        public router: Router,
        private navCtrl: NavController,
        public historyService: SaleService,
        private storage: Storage) {
        super(historyService);
    }

    async  ionViewWillEnter() {
        this.user = await this.storage.get('user');
        this.route.data.forEach(res => {
            this.toItems = res['data']['entity']['to'];
            this.list = res['data']['entity']['data'];
            this.params['page'] = res['data']['entity']['current_page'];
            this.totalItems = res['data']['entity']['total'];
        });
    }

    createSale() {
        this.navCtrl.navigateRoot(['/app/tabs/profile/add-sale']);
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }

}
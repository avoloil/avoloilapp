import { FavoriteService } from './favorite/shared/services/favorite.service';
import { FavoriteComponent } from './favorite/favorite';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProfileComponent } from "./profile.component";
import { HistoryComponent } from "./history/history.component";
import { ShopComponent } from "./shop/shop.component";
import { ShopService } from "./shop/shared/services/shop.service";
import { SaleComponent } from "./sale/sale.component";
import { SaleService } from "./sale/shared/services/sale.service";
import { AssortsComponent } from "./assorts/assorts.component";
import { AssortsService } from "./assorts/shared/services/assorts.service";
import { AddSaleComponent } from "./add-sale/add-sale.component";
import { AddSaleService } from "./add-sale/shared/services/add-sale.service";
import { HistoryService } from "./history/shared/services/history.service";
import { ProfileService } from "./shared/services/profile.service";
import { UserComponent } from "./profile/profile.component";
// import { HistoryService } from "./history/shared/services/history.service";

const routes: Routes = [
    {
        path: '',
        component: ProfileComponent,
        resolve: {
            data: ProfileService
        }
    },
    {
        path: 'profile',
        component: UserComponent,
        resolve: {
            data: ProfileService
        }
    },
    {
        path: 'favorite',
        component: FavoriteComponent,
        // resolve: {
        //     data: FavoriteService
        // }
    },
    {
        path: 'history',
        component: HistoryComponent,
        resolve: {
            data: HistoryService
        }
    },
    {
        path: 'shop',
        component: ShopComponent,
        // resolve: {
        //     data: ShopService
        // }
    },
    {
        path: 'sale',
        component: SaleComponent,
        resolve: {
            data: SaleService
        }
    },
    {
        path: 'add-sale',
        component: AddSaleComponent,
        resolve: {
            data: AddSaleService
        }
    },
    {
        path: 'assorts',
        component: AssortsComponent,
        resolve: {
            data: AssortsService
        }
    },
    {
        path: 'action-and-bonuce',
        loadChildren: './action-and-bonuce/action-and-bonuce.module#ActionAndBonuceModule'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRouterModule { }
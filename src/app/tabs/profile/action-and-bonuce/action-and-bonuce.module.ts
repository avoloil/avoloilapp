import { FirebaseModule } from './../../../shared/firebase.module';
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "src/app/shared/shared.module";
import { ActionAndBounceRoutingModule } from "./action-and-bonuce-routing.module";
import { MomentModule } from 'angular2-moment';
import { PointListComponent } from "./point-list/point-list.component";
import { PointListService } from "./point-list/shared/services/point-list.service";
import { ItemInfoComponent } from './item-info/item-info.component';
import { ItemInfoService } from "./item-info/shared/services/item-info.service";
import { PartnerListComponent } from './form-gift/partner-list.component';
import { PartnerListService } from "./form-gift/shared/services/partner-list.service";
import { KazPointComponent } from "./kaz-point/kaz-point.component";
import { CountryGuard } from "./shared/guard/country-guard.service";
import { SMS } from '@ionic-native/sms/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { QRCodeComponent } from "./qr-code/qr-code.component";
import { ProfileService } from '../shared/services/profile.service';

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        ActionAndBounceRoutingModule,
        MomentModule,
        FirebaseModule
    ],
    declarations: [
        PointListComponent,
        ItemInfoComponent,
        PartnerListComponent,
        KazPointComponent,
        QRCodeComponent
    ],
    providers: [
        PointListService,
        ItemInfoService,
        PartnerListService,
        CountryGuard,
        SMS,
        AndroidPermissions,
        ProfileService
    ],
    entryComponents: [
        QRCodeComponent
    ]
})
export class ActionAndBonuceModule {

}
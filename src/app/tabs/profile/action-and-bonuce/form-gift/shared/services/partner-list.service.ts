import { Injectable } from "@angular/core";
import { RequestService } from "src/app/shared/services/request.service";
import { EntityService } from "src/app/shared/base/entity.service";
import { ToastController } from "@ionic/angular";
import { tap } from "rxjs/operators";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class PartnerListService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'partner';
    }

    resolve(route: ActivatedRouteSnapshot) {
        return this.get(null, { city: localStorage['city'] });
    }

    selectPartner(merchant_id: number, partner_id: number) {
        let url = this.url('select');
        url += `/${merchant_id}/partner/${partner_id}`;
        return this.request.post(url, {}).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Спасибо, Заказ принят! Мерчант будет отправлен в выбранный магазин');
                }
            }));
        // merchant/1/partner/1
    }
}
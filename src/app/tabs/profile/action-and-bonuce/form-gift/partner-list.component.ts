import {Storage} from '@ionic/storage';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PartnerListService} from './shared/services/partner-list.service';
import {InfinitiScroll} from 'src/app/shared/components/infiniti-scrolle.component';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';

@Component({
    selector: 'app-partner-list',
    templateUrl: './partner-list.component.html',
    styleUrls: ['./partner-list.component.scss']
})
export class PartnerListComponent extends InfinitiScroll implements OnInit {


    private entity: object = {
        name: '',
        email: '',
        phone: ''
    };
    private unreadCount: object = {};
    private user: object = {};
    private success: boolean = false;
    private merchant_id: number;
    private chatCollection: AngularFireList<any>;
    private unreadList: any;
    private count: number = 0;
    public blockSelectBtn = false;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private storage: Storage,
                private db: AngularFireDatabase,
                private _parnter: PartnerListService) {
        super(_parnter);
    }

    ngOnInit() {
        this.params['city'] = localStorage['city'];
        this.merchant_id = this.route.snapshot.queryParams['merchant_id'];
        this.route.data.forEach(res => {
            this.toItems = res['data']['entity']['to'];
            this.list = res['data']['entity']['data'];
            this.params['page'] = res['data']['entity']['current_page'];
            this.totalItems = res['data']['entity']['total'];
        });
        this.storage.get('user').then(user => this.user = user);
    }


    selectPartner(partner_id: number) {
        if (this.blockSelectBtn) {
            return false;
        }
        this.blockSelectBtn = true;
        this.count = 0;
        return this._parnter.selectPartner(this.merchant_id, partner_id).subscribe(res => {
            this.blockSelectBtn = false;
            if (res['status']) {
                this.subscribeToChat(res['entity'], res['merchant_title']);
            }
            this.router.navigate(['/app/tabs/profile/action-and-bonuce']);
        });
    }

    subscribeToChat(arrChats, title) {
        if (arrChats.length > this.count - 1) {
            this.unreadCount = {};
            this.chatCollection = this.db.list('chats/' + arrChats[this.count]['chat_id'] + '/messages');
            this.unreadList = this.db.object('chats/' + arrChats[this.count]['chat_id'] + '/unread');
            this.unreadList.snapshotChanges().subscribe(action => {
                console.log(action.payload.val());
                if (action.payload.val() != this.unreadCount) {
                    action.payload.val() != null ? this.unreadCount = action.payload.val() : this.unreadCount = {};
                    console.log(action.payload.val());
                    // tslint:disable-next-line: max-line-length
                    this.unreadCount[arrChats[this.count]['user_id']] ? this.unreadCount[arrChats[this.count]['user_id']]++ : this.unreadCount[arrChats[this.count]['user_id']] = 1;
                    if (this.count) delete this.unreadCount[arrChats[this.count]['user_id']];
                    this.unreadCount['room_id'] = arrChats[this.count]['chat_id'];
                    this.sendMessage(arrChats[this.count]['chat_id'], title);
                    this.unreadMessage();
                    this.count++;
                    this.subscribeToChat(arrChats, title);
                } else {
                    this.count++;
                }
            });
        } else {
            this.router.navigate(['/app/tabs/profile/action-and-bonuce']);
        }
    }

    sendMessage(chat_id, title) {
        let msg = {
            chat_user_id: localStorage['userId'],
            date: new Date().toString(),
            text: `${this.user['username']} ${this.user['lastname']}, id: ${this.user['id']}, название: ${title}`,
            unread: true,
            room_id: chat_id,
            color: 'green'
        };
        this.chatCollection.push(msg);
    }

    unreadMessage() {
        this.unreadList.update(this.unreadCount);
    }

    back() {
        window.history.back();
    }

}

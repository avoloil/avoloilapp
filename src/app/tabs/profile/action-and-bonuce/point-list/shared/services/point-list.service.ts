import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { Resolve } from "@angular/router";
import { ProfileService } from "src/app/tabs/profile/shared/services/profile.service";
import { concat } from "rxjs";
import { toArray } from "rxjs/operators";

@Injectable()
export class PointListService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController, private _profile: ProfileService) {
        super(request, toastController);
        this.service_name = 'merchant';
    }

    resolve() {
        // return this.get();

        const mer = this.get();
        const user = this._profile.get(localStorage['userId']);

        return new Promise((resolve) => {
            concat(mer, user)
                .pipe(toArray())
                .subscribe(x => {
                    resolve(x);
                });
        });
    }

    // getAll() {

    // }
}
import { UserUpdateService } from './../../../../shared/providers/user-update';
import { Component, OnInit } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { PointListService } from "./shared/services/point-list.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import { AlertController, NavController, ModalController } from "@ionic/angular";
import { BarcodeScannerOptions, BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { QRCodeComponent } from "../qr-code/qr-code.component";
import { ProfileService } from "../../shared/services/profile.service";

@Component({
    selector: 'app-profile-point-list',
    templateUrl: './point-list.component.html',
    styleUrls: ['./point-list.component.scss']
})
export class PointListComponent extends InfinitiScroll implements OnInit {

    barcodeScannerOptions: BarcodeScannerOptions;

    private user: object;
    encodeData: any;
    private role;
    // public entity = {
    //     id: 12314,
    //     points: 1440,
    // };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        public pointsService: PointListService,
        private storage: Storage,
        private _profile: ProfileService,
        private navCtrl: NavController,
        private modalController: ModalController,
        private barcodeScanner: BarcodeScanner,
        private updateUser: UserUpdateService
    ) {
        super(pointsService);
    }

    async ngOnInit() {
        // this.user = await this.storage.get('user');
        this.updateUser.user.subscribe(user => user != null ? this.user = user : this.user);
        this.route.data.forEach(res => {
            this.toItems = res['data'][0]['entity']['to'];
            this.list = res['data'][0]['entity']['data'];
            this.params['page'] = res['data'][0]['entity']['current_page'];
            this.totalItems = res['data'][0]['entity']['total'];
            this.user = res['data'][1]['entity'];
        }); console.log(this.user);
        this.user['role'] === "USER" ? this.role = true : this.role = false;
    }

    encodedId() {
        this.presentAlert();
    }

    async presentAlert() {
        const modal = await this.modalController.create({
            component: QRCodeComponent,
            componentProps: {
                qrCode: this.user['qr_code']
                //     'prop1': value,
                //     'prop2': value2
            }
        });
        await modal.present();

    }

    openInfo(id: number) {
        console.log(id);
        this.navCtrl.navigateRoot(['/app/tabs/profile/action-and-bonuce/info', id]);
    }

    refreshUser() {
        this._profile.get(localStorage['userId']).subscribe(res => {
            this.user = res['entity'];
        });
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }
}
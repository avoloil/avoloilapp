import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { tap } from "rxjs/operators";

@Injectable()
export class ItemInfoService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'merchant';
    }

    resolve(route: ActivatedRouteSnapshot) {
        let id = route.params['id'];
        return this.get(id);
    }

    buy(id: number) {
        let url = this.url('index');
        url += '/' + id + '/buy';
        return this.request.post(url, {}).pipe(
            tap(res => {
                if (!res['status']) {
                    this.presentToastWithOptions('danger', 'Произошла ошибка');
                }
            })
        );
    }
}
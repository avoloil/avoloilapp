import {UserUpdateService} from './../../../../shared/providers/user-update';
import {ProfileService} from 'src/app/tabs/profile/shared/services/profile.service';
import {Storage} from '@ionic/storage';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ItemInfoService} from './shared/services/item-info.service';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-item-info',
    templateUrl: './item-info.component.html',
    styleUrls: ['./item-info.component.scss']
})
export class ItemInfoComponent implements OnInit {

    private entity: object = {};
    private user: object = {};

    constructor(private _itemInfo: ItemInfoService,
                private route: ActivatedRoute,
                private _userP: UserUpdateService,
                private navCtrl: NavController,
                private _profile: ProfileService,
                private storage: Storage,
                private router: Router) {
    }

    async ngOnInit() {
        this.entity = this.route.snapshot.data['data']['entity'];
        this._profile.get(localStorage['userId']).subscribe(res => {
            this.user = res['entity'];
            this._userP.updateData(this.user);

        });
    }

    openFormGift() {
        // this.router.navigate(['/app/tabs/profile/action-and-bonuce/form-gift']);
    }

    buy(id: number) {
        this._itemInfo.buy(id).subscribe(res => {
            if (res['status']) {
                this.navCtrl.navigateRoot(['/app/tabs/profile/action-and-bonuce/partner-list'], {queryParams: {merchant_id: id}});
            }
        });
    }

    back() {
        this.router.navigate(['/app/tabs/profile/action-and-bonuce']);
    }

}

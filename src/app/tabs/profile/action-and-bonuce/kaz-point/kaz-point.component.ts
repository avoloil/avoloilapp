import {Component} from '@angular/core';
// import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import {ActivatedRoute, Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ModalController, NavController} from '@ionic/angular';
import {BarcodeScanner, BarcodeScannerOptions} from '@ionic-native/barcode-scanner/ngx';
import {SMS} from '@ionic-native/sms/ngx';
import {QRCodeComponent} from '../qr-code/qr-code.component';
import {ProfileService} from '../../shared/services/profile.service';

@Component({
    selector: 'app-profile-kaz-point',
    templateUrl: './kaz-point.component.html',
    styleUrls: ['./kaz-point.component.scss']
})
export class KazPointComponent {

    barcodeScannerOptions: BarcodeScannerOptions;

    private user: object;
    encodeData: any;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private storage: Storage,
                private navCtrl: NavController,
                private sms: SMS,
                private profile: ProfileService,
                private modalController: ModalController,
                private barcodeScanner: BarcodeScanner) {
        // super(pointsService);
    }

    async ionViewWillEnter() {
        this.profile.get(localStorage['userId']).subscribe(res => {
            this.user = res['entity'];
            if (res['entity']['active_promo'] && res['entity']['active_promo']['status'] == 0) {
                this.user['discount_percent'] += 2;
            }
        });
    }

    encodedId() {
        this.presentAlert();
    }

    async presentAlert() {
        const modal = await this.modalController.create({
            component: QRCodeComponent
            // componentProps: {
            //     'prop1': value,
            //     'prop2': value2
            // }
        });

        await modal.present();
    }

    sendFile(title: string) {
        this.sms.send('+7', `Скидка на масло AVOL. Используй этот разовый промокод при покупке: ${title} AppStore: ISO  GooglePlay: Android`);
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }
}
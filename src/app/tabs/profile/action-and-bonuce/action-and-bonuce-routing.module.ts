import { ProfileService } from 'src/app/tabs/profile/shared/services/profile.service';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PointListComponent } from "./point-list/point-list.component";
import { PointListService } from "./point-list/shared/services/point-list.service";
import { ItemInfoComponent } from "./item-info/item-info.component";
import { ItemInfoService } from "./item-info/shared/services/item-info.service";
import { KazPointComponent } from "./kaz-point/kaz-point.component";
import { CountryGuard } from "./shared/guard/country-guard.service";
import { PartnerListService } from "./form-gift/shared/services/partner-list.service";
import { PartnerListComponent } from "./form-gift/partner-list.component";
const routes: Routes = [
    {
        path: '',
        component: PointListComponent,
        canActivate: [CountryGuard],
        resolve: {
            data: PointListService
        }
    },
    {
        path: 'info/:id',
        component: ItemInfoComponent,
        resolve: {
            data: ItemInfoService
        }
    },
    {
        path: 'kaz-point',
        component: KazPointComponent
    },
    {
        path: 'partner-list',
        component: PartnerListComponent,
        resolve: {
            data: PartnerListService
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActionAndBounceRoutingModule { }
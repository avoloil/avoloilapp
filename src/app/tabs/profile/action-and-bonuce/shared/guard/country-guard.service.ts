import { Injectable } from "@angular/core";
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from "@angular/router";
import { Observable } from "rxjs";
import { AlertController } from "@ionic/angular";

@Injectable()
export class CountryGuard implements CanActivate {

    constructor(
        public alertController: AlertController,
        private router: Router) {
    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (localStorage['country'] === 'Казахстан') {
            this.router.navigate(['app/tabs/profile/action-and-bonuce/kaz-point']);
            return false;
        } else {
            return true;
        }
    }
}


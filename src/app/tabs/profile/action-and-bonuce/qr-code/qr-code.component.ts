import { Component } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";
import { ProfileService } from "../../shared/services/profile.service";

@Component({
    selector: 'app-qr-code',
    templateUrl: './qr-code.component.html',
    styleUrls: ['./qr-code.component.scss']
})
export class QRCodeComponent {

    qrCode: any;

    constructor(private params: NavParams, private modalController: ModalController, private _profile: ProfileService) {

    }

    ionViewWillEnter() {
        this._profile.get(localStorage['userId']).subscribe(res => {
            this.qrCode = res['entity']['qr_code'];
            console.log(this.qrCode);
        });
    }

    back() {
        this.modalController.dismiss();
    }
}
import { Component } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { FavoriteService } from "./shared/services/favorite.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import * as queryString from 'query-string';
import { NavController, Platform } from "@ionic/angular";
import { OilInfoService } from "../../oil/oil-info/shared/oil-info.service";

@Component({
    selector: 'app-user-favorite',
    templateUrl: './favorite.html',
    styleUrls: ['./favorite.scss']
})
export class FavoriteComponent extends InfinitiScroll  {

    public index: number;
    public filter: object;
    private status: boolean = false;


    constructor(
        public router: Router,
        private navCtrl: NavController,
        public route: ActivatedRoute,
        public favoriteService: FavoriteService,
        private _oilInfo: OilInfoService,
        private platform: Platform,
        public storage: Storage) {
        super(favoriteService);
        this.favorite = true;
    }

    ionViewWillEnter() {
        this.favoriteService.get().subscribe(res => {
            this.toItems = res['entity']['to'];
            this.list = res['entity']['data'];
            this.list.map(el => el['favorite'] = true);
            this.params['page'] = res['entity']['current_page'];
            this.totalItems = res['entity']['total'];
        });
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }

    openInfo(id: number, info: number) {
        console.log(id);
        this.navCtrl.navigateRoot([`/app/tabs/oil/list/info`, id], { queryParams: { info } });
    }
    addWish(id: number, index: number) {
        this._oilInfo.addWish(id).subscribe(res => {
            if (res['status']) {
                this.list[index]['favorite'] = 1;
                this.list[index]['status'] = 1;
            }
        });
    }

    minusWish(id: number, index: number) {
        this._oilInfo.minusWish(id).subscribe(res => {
            if (res['status']) {
                this.list[index]['favorite'] = 0;
                this.list[index]['status'] = 0;
            }
        });
    }

}

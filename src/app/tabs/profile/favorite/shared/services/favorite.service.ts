import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController, Platform } from "@ionic/angular";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class FavoriteService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController, private platform: Platform) {
        super(request, toastController);
        this.service_name = 'favorite';
    }

    resolve(route: ActivatedRouteSnapshot) {
        return this.get();
    }
}
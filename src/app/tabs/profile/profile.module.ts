import {FirebaseModule} from 'src/app/shared/firebase.module';
import {FavoriteService} from './favorite/shared/services/favorite.service';
import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {SharedModule} from 'src/app/shared/shared.module';
import {ProfileRouterModule} from './profile-router.module';
import {ProfileComponent} from './profile.component';
import {HistoryService} from './history/shared/services/history.service';
import {ProfileService} from './shared/services/profile.service';
import {HistoryComponent} from './history/history.component';
import {MomentModule} from 'angular2-moment';
import {ShopComponent} from './shop/shop.component';
import {ShopService} from './shop/shared/services/shop.service';
import {SaleComponent} from './sale/sale.component';
import {SaleService} from './sale/shared/services/sale.service';
import {AssortsComponent} from './assorts/assorts.component';
import {AssortsService} from './assorts/shared/services/assorts.service';
import {AddSaleComponent} from './add-sale/add-sale.component';
import {AddSaleService} from './add-sale/shared/services/add-sale.service';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {FormsModule} from '@angular/forms';
import {UserComponent} from './profile/profile.component';
import {PopoverProfile} from './shared/components/popover-profile/popover-profile';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {ShopInfoService} from '../map/shop-info/shared/services/shop.service';
import {AuthService} from 'src/app/auth/shared/services/auth.service';
import {FavoriteComponent} from './favorite/favorite';
import {OilInfoService} from '../oil/oil-info/shared/oil-info.service';
import {DataNotService} from 'src/app/shared/providers/data-not-count.service';
import {BonuceModalComponent} from 'src/app/tabs/profile/add-sale/shared/component/bonuce-modal/bonuce-modal.component';
import {ShopInfoModule} from '../map/shop-info/shop-info.module';
import {SocialLoginService} from '../../auth/shared/services/social-login.service';
import {Facebook} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        ProfileRouterModule,
        MomentModule,
        FormsModule,
        FirebaseModule,
        ShopInfoModule
    ],
    declarations: [
        ProfileComponent,
        HistoryComponent,
        ShopComponent,
        SaleComponent,
        AssortsComponent,
        AddSaleComponent,
        UserComponent,
        PopoverProfile, BonuceModalComponent,
        FavoriteComponent
    ],
    providers: [
        HistoryService,
        ProfileService,
        ShopService,
        SaleService,
        AssortsService,
        AddSaleService,
        BarcodeScanner,
        CallNumber,
        ShopInfoService,
        AuthService,
        FavoriteService,
        OilInfoService,
        SocialLoginService,
        Facebook, GooglePlus],
    entryComponents: [PopoverProfile, BonuceModalComponent]
})
export class ProfileModule {
}
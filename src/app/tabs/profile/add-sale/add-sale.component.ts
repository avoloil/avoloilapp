import { AddSaleService } from './shared/services/add-sale.service';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { ProfileService } from './../shared/services/profile.service';
import { Component, OnInit } from '@angular/core';
import { InfinitiScroll } from 'src/app/shared/components/infiniti-scrolle.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AssortsService } from '../assorts/shared/services/assorts.service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { BonuceModalComponent } from './shared/component/bonuce-modal/bonuce-modal.component';

@Component({
    selector: 'app-tabs-add-sale',
    templateUrl: './add-sale.component.html',
    styleUrls: ['./add-sale.component.scss']
})
export class AddSaleComponent extends InfinitiScroll {

    encodeData: any;
    scannedData: {};
    barcodeScannerOptions: BarcodeScannerOptions;
    // public tableHeader: Array<string> = [];
    private user: object;
    private userId: number | string;
    public addSale = true;
    private block: boolean = false;

    constructor(private route: ActivatedRoute,
        public router: Router,
        private _profile: ProfileService,
        private alertController: AlertController,
        private modalController: ModalController,
        public _assortService: AssortsService,
        private toastController: ToastController,
        public _addSale: AddSaleService,
        private storage: Storage,
        private barcodeScanner: BarcodeScanner) {
        super(_addSale);
        this.pageUrl = true;
        this.encodeData = 'https://www.FreakyJolly.com';
        //Options
        this.barcodeScannerOptions = {
            showTorchButton: true,
            showFlipCameraButton: true
        };
    }

    async ionViewWillEnter() {
        this.user = await this.storage.get('user');
        this.route.data.forEach(res => {
            this.toItems = res['data']['entity']['to'];
            this.list = res['data']['entity']['data'];
            this.list.map(el => el['count'] = 0);
            this.params['page'] = res['data']['entity']['current_page'];
            this.totalItems = res['data']['entity']['total'];
        });
        this.userSell = {
            canisters: [],
            user_id: null,
        };
        this.addCount();
    }

    scanCode() {
        const that = this;
        this.barcodeScanner
            .scan()
            .then(barcodeData => {
                that.userSell.user_id = barcodeData.text;
            })
            .catch(err => {
                console.log('Error', err);
            });
    }

    addItem(id: number, count: number, index: number, title, volume) {
        this.list[index]['count']++;
        let x = this.userSell.canisters.findIndex(el => el['canister'] === id);
        if (x === -1) {
            this.userSell.canisters.push({
                canister: id, count: 1, title, bonus_volume: 0, volume: Number(volume)
            });
        } else {
            this.userSell.canisters[x]['count']++;
        }
    }

    minusItem(id: number, count: number, index: number) {
        let x = this.userSell.canisters.findIndex(el => el['canister'] === id);
        if (x !== -1) {
            if (this.userSell.canisters[x]['count']) {
                this.list[index]['count']--;
                this.userSell.canisters[x]['count']--;
                if (!this.userSell.canisters[x]['count']) {
                    this.userSell.canisters.splice(x, 1);
                }
            }
        }
    }

    addCount() {
        this.list.map(el => {
            if (!el.count) {
                el.count = 0;
            }
        });
    }

    refresh() {
        // setTimeout(() => {
        // });
    }

    sale() {
        this.block = true;
        if (this.userSell['user_id']) {
            this._profile.getUser(this.userSell['user_id']).subscribe(user => {
                this.block = false;
                if (user['entity']['discount_liter']) {
                    this.presentAlertCheckbox(user['entity']['discount_liter']);
                } else {
                    this.saleDone();
                }
            }, err => {
                this.block = false;
            });
        }
    }

    async presentAlertCheckbox(discount_liter) {
        const modal = await this.modalController.create({
            component: BonuceModalComponent,
            cssClass: 'modalPopup',
            componentProps: { canisters: this.userSell.canisters, discount_liter }
        });
        modal.onDidDismiss().then(res => {
            this.block = false;
            if (res['data']) {
                this.userSell.canisters = res['data'];
            }
            this.saleDone();
        });
        return await modal.present();
    }

    saleDone() {
        this.block = true;
        this._assortService.sale(this.userSell).subscribe(res => {
            if (res['status']) {
                this.block = false;
                this.userSell.canisters.map(el => {
                    if (el['count'] > 1) {
                        if (el['volume'] * el['count'] < el['bonus_volume']) {
                            // tslint:disable-next-line: max-line-length
                            this.presentToastWithOptions(`С бонусного счета списано ${el['volume'] * el['count']} л , остаток на счету составляет ${el['bonus_volume'] - (el['volume'] * el['count'])} л`);
                        }
                    } else {
                        if (el['volume'] < el['bonus_volume']) {
                            // tslint:disable-next-line: max-line-length
                            this.presentToastWithOptions(`С бонусного счета списано ${el['volume']} л , остаток на счету составляет ${el['bonus_volume'] - el['volume']} л`);
                        }
                    }
                });
            }
            if (res['status']) {
                this.list.map(el => el['count'] = 0);
                this.userSell.canisters = [];
            }
        }, err => {
            this.block = false;
        });
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }

    async presentToastWithOptions(message) {
        const toast = await this.toastController.create({
            // header: 'Toast header',
            duration: 2000,
            message,
            position: 'top',
            color: 'success',
            cssClass: 'normalToast'
        });
        toast.present();
    }

}
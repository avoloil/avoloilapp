import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { Resolve } from "@angular/router";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";

@Injectable()
export class AddSaleService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'add-sale';
    }

    resolve() {
        return this.getAll();
    }

    getAll() {
        let url = this.url('index');
        url += '/' + localStorage['partnerId'] + '/getCanisters';
        return this.request.get(url);
    }
}
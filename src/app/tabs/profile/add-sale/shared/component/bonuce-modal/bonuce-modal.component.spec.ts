import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonuceModalComponent } from './bonuce-modal.component';

describe('BonuceModalComponent', () => {
  let component: BonuceModalComponent;
  let fixture: ComponentFixture<BonuceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonuceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonuceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component } from "@angular/core";
import { NavParams, ModalController } from "@ionic/angular";

@Component({
  selector: 'app-bonuce-modal',
  templateUrl: './bonuce-modal.component.html',
  styleUrls: ['./bonuce-modal.component.scss']
})
export class BonuceModalComponent {

  private canIndex: number;
  private bonus_volume: any;
  private block: boolean = false;

  constructor(private modalController: ModalController, private params: NavParams) {
    console.log(params);

  }

  select(ev: any) {
    console.log(ev['detail']['value']);
    this.canIndex = ev['detail']['value']['index'];
  }

  send() {
    this.block = true;
    this.params.data['canisters'][this.canIndex]['bonus_volume'] = this.bonus_volume;
    this.modalController.dismiss(this.params.data['canisters']);
  }

  maxNumber(ev: any) {
    setTimeout(() => {
      if (Number(ev['detail']['value']) > this.params.data['discount_liter']) {
        this.bonus_volume = parseInt(this.params.data['discount_liter']);
        ev['detail']['value'] = parseInt(this.params.data['discount_liter']);
      }
    });
  }

}

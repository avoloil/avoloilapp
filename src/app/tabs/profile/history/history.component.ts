import { Component } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { HistoryService } from "./shared/services/history.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";

@Component({
    selector: 'app-tabs-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.scss']
})
export class HistoryComponent extends InfinitiScroll {

    public tableHeader: Array<string> = ['Дата', 'Количество', 'Название'];
    private user: object;

    constructor(
        private route: ActivatedRoute,
        public router: Router,
        public historyService: HistoryService,
        private storage: Storage) {
        super(historyService);
    }

    async  ionViewWillEnter() {
        this.user = await this.storage.get('user');
        this.route.data.forEach(res => {
            this.toItems = res['data']['entity']['to'];
            this.list = res['data']['entity']['data'];
            this.params['page'] = res['data']['entity']['current_page'];
            this.totalItems = res['data']['entity']['total'];
        });
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }

}
import { Component } from "@angular/core";
import { ShopService } from "./shared/services/shop.service";
import { ActivatedRoute, Router } from "@angular/router";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { NavController, ModalController } from "@ionic/angular";
import { ShopInfoService } from "../../map/shop-info/shared/services/shop.service";
import { ShopInfoPage } from "../../map/shop-info/shop-info.page";

@Component({
    selector: 'app-tabs-profile-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.scss']
})
export class ShopComponent extends InfinitiScroll {

    private user: object;

    constructor(
        public route: ActivatedRoute,
        public router: Router,
        private navCtrl: NavController,
        private callNumber: CallNumber,
        private _shopInfo: ShopInfoService,
        private modalController: ModalController,
        public shopService: ShopService) {
        super(shopService);
    }

    async ionViewWillEnter() {
        this.shopService.get().subscribe(res => {
            this.toItems = res['entity']['to'];
            this.list = res['entity']['data'];
            this.params['page'] = res['entity']['current_page'];
            this.totalItems = res['entity']['total'];
        });
    }

    call(phone) {
        this.callNumber.callNumber(phone, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    async openMap(shopId) {
        const modal = await this.modalController.create({
            component: ShopInfoPage,
            componentProps: { shopId }
        });

        modal.present();
    }

    openChat(user_id) {
        this._shopInfo.createChat(user_id).subscribe(res => {
            console.log(res);
            if (res['status']) {
                this.navCtrl.navigateRoot(['/app/tabs/dialog', res['entity']['id']]);
            }
        });
    }

    rmFavorite(userId) {
        this._shopInfo.rmFavorite(userId).subscribe(() => { this.params['page'] = 1; this.list = []; this.getAll(); });
    }

    back() {
        this.router.navigate(['/app/tabs/profile']);
    }
}
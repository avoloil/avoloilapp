import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class ShopService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'shopFavorite';
    }

    // resolve(route: ActivatedRouteSnapshot) {
    //     this.get(route.params['id']);
    // }

    resolve(route: ActivatedRouteSnapshot) {
        return this.get();
    }

    // createChat(user_id: number) {
    //     let url = this.url('chat');
    //     url += '/' + user_id;
    //     return this.request.get(url);
    // }


}
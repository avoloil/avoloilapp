import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {ModalController, AlertController} from '@ionic/angular';
import {ProfileService} from '../shared/services/profile.service';
import {ModalSelecteCity} from 'src/app/shared/components/modal-selecte-city/modal-selecte-city';
import {Storage} from '@ionic/storage';
import {SocialLoginService} from '../../../auth/shared/services/social-login.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class UserComponent {

    public reg: FormGroup;
    // public roles: any;
    // customAlertOptions: any = {
    //     header: 'Роль',
    //     subHeader: 'Выберите роль пользователя',
    //     translucent: true
    // };

    customHeader: any = {
        header: 'Страна'
    };

    private pwdPattern = '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}';
    emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

    constructor(
        private formBuilder: FormBuilder,
        public router: Router,
        public socialLoginService: SocialLoginService,
        private route: ActivatedRoute,
        public profile: ProfileService,
        private modalController: ModalController,
        private alertController: AlertController,
        private storage: Storage
    ) {
        this.reg = this.formBuilder.group({
            id: new FormControl(null),
            username: new FormControl(''),
            lastname: new FormControl(''),
            city: new FormControl(''),
            country_id: new FormControl(null),
            mobile: new FormControl(''),
            email: new FormControl(''),
            password: new FormControl(''),
            passwordConfirm: new FormControl(''),
            role: new FormControl('USER'),
            lat: new FormControl(null),
            lng: new FormControl(null)
        });
    }

    async ionViewWillEnter() {
        this.reg.patchValue(this.route.snapshot.data['data']['entity']);
    }

    regForm() {
        this.reg.value.email.toLowerCase();
        this.profile.update(this.reg.value).subscribe(res => {
            if (res['status']) {
                if (res['user']['partner']) {
                    localStorage['partnerId'] = res['user']['partner']['id'];
                }
                localStorage['latitude'] = res['user']['lat'];
                localStorage['longitude'] = res['user']['lng'];
                localStorage['user'] = JSON.stringify(res['user']);
                localStorage['userId'] = res['user']['id'];
                localStorage['country'] = res['user']['country'];
                localStorage['city'] = res['user']['city'];
                this.storage.set('user', res['user']);
                this.router.navigate(['/app/tabs/profile']);
            }
        });
    }

    back() {
        this.router.navigate(['app/tabs/profile']);
    }

    openModal() {
        this.presentModal();
    }

    async presentModal() {
        const modal = await this.modalController.create({
            component: ModalSelecteCity
        });
        modal.onDidDismiss().then(res => {
            this.reg.patchValue({city: res['data']['city'], lat: res['data']['latitude'], lng: res['data']['longitude']});
            localStorage['latitude'] = res['data']['latitude'];
            localStorage['longitude'] = res['data']['longitude'];
            console.log(res['data']['country']);
            switch (res['data']['country']) {
                case 'Беларусь': {
                    this.reg.patchValue({country_id: 2});
                    break;
                }
                case 'Россия': {
                    this.reg.patchValue({country_id: 1});
                    break;
                }
                case 'Казахстан': {
                    this.reg.patchValue({country_id: 3});
                    break;
                }
                case '100000': {
                    this.reg.patchValue({country_id: 3});
                }
            }
        });
        return await modal.present();
    }

    deleteUser() {
        this.presentAlertConfirm();
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            header: 'Внимание!',
            message: 'Вы точно хотите удалить аккаунт?',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Вход',
                    handler: () => {
                        this.profile.deleteOnce({id: localStorage['userId']}).subscribe();
                        localStorage.clear();
                        this.storage.clear();
                        this.socialLoginService.logoutGoogle()
                            .finally((res: any) => {
                                console.log(res);
                            });
                        this.router.navigate(['/app/tabs']);
                        console.log('Confirm Okay');
                    }
                }
            ]
        });
        await alert.present();

    }
}

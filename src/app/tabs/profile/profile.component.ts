import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ActionSheetController, AlertController, NavController, PopoverController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { PopoverProfile } from './shared/components/popover-profile/popover-profile';
import { ProfileService } from './shared/services/profile.service';
import { AuthService } from 'src/app/auth/shared/services/auth.service';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { DataNotService } from 'src/app/shared/providers/data-not-count.service';
import { UserUpdateService } from 'src/app/shared/providers/user-update';

@Component({
    selector: 'app-tabs-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {

    public user: object = {};
    private car: string;
    private entity: object = {};
    private unreadList: any;
    private count: number = 0;
    private startPage: boolean = true;
    private unreadTriger$: AngularFireObject<any>;
    private trigerCount = 0;
    unreadArr: Array<object> = [];

    constructor(private route: ActivatedRoute,
        public router: Router,
        private countNot: DataNotService,
        public storage: Storage,
        private navCtrl: NavController,
        private _userP: UserUpdateService,
        private auth: AuthService,
        private _profile: ProfileService,
        private db: AngularFireDatabase,
        public actionSheetController: ActionSheetController,
        private alertController: AlertController,
        public popoverController: PopoverController) {
    }

    ionViewWillEnter() {
        // this.user = this.route.snapshot.data['data']['entity'];
        const userData = JSON.parse(localStorage.getItem('user'));
        this._profile.get(userData.id).subscribe(user => {
            this.user = user['entity'];
            if (this.user['partner']) {
                localStorage['partnerId'] = this.user['partner']['id'];
            }
            localStorage['latitude'] = this.user['lat'];
            localStorage['longitude'] = this.user['lng'];
            localStorage['user'] = JSON.stringify(this.user);
            localStorage['userId'] = this.user['id'];
            localStorage['country'] = this.user['country'];
            localStorage['city'] = this.user['city'];
            this.storage.set('user', this.user);
            this.startPage = true;
            this.unreadAll();
            this._userP.updateData(this.user);
        });
    }

    async unreadAll() {
        this.count = 0;
        this.user['chats'].map((el, index) => {
            this.unreadList = this.db.list('chats/' + el);
            this.unreadList.valueChanges()
                .subscribe(data => {
                    if (data.length > 1) {
                        if (data[1]) {
                            let roomIndex = this.unreadArr.findIndex(el => el['room_id'] == data[1]['room_id']);
                            if (roomIndex !== -1) {
                                this.unreadArr[roomIndex] = data[1];
                                this.sumUnread();
                            } else {
                                this.unreadArr.push(data[1]);
                                this.sumUnread();
                            }
                            console.log(this.unreadArr);
                        }
                    }
                });
        });
    }

    async sumUnread() {
        this.count = 0;
        this.unreadArr.map(el => {
            if (el[this.user['id']]) {
                this.count += el[this.user['id']];
            }
        });
    }

    changePush(ev: any) {
        console.log(ev.detail['checked']);
        this._profile.updateNot({ notification: ev.detail['checked'], id: this.user['id'] }).subscribe();
    }

    changeOil(ev: any) {
        console.log(ev.detail);
    }

    openAction() {
        this.navCtrl.navigateRoot(['/app/tabs/profile/action-and-bonuce']);
    }

    openChatList() {
        this.router.navigate(['/app/tabs/chat-list']);
    }

    openShop() {
        this.router.navigate(['/app/tabs/profile/shop']);
    }

    openSale() {
        this.router.navigate(['/app/tabs/profile/sale']);
    }

    openAssort() {
        this.navCtrl.navigateRoot(['/app/tabs/profile/assorts']);
    }

    exit() {
        localStorage.clear();
        this.storage.clear();
        this.countNot.updateData(0);
        this.auth.logout().subscribe();
        this.router.navigate(['/app/tabs/oil']);
    }

    async presentActionSheet() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Выберите вариант',
            buttons: [
                {
                    text: 'История покупок',
                    handler: () => {
                        this.router.navigate(['/app/tabs/profile/history']);
                    }
                },
                {
                    text: 'Мои желания',
                    handler: () => {
                        this.storage.set('favorite', true);
                        this.navCtrl.navigateRoot(['/app/tabs/profile/favorite']);
                    }
                },
                {
                    text: 'Отмена',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }]
        });
        await actionSheet.present();
    }

    async addCar() {
        const alert = await this.alertController.create({
            header: 'Добавить',
            inputs: [
                {
                    name: 'mark',
                    placeholder: 'Марка',
                    type: 'text'
                },
                {
                    name: 'model',
                    type: 'text',
                    placeholder: 'Модель'
                }
            ],
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (e) => {
                        console.log(e, 'Confirm Cancel');
                    }
                }, {
                    text: 'Принять',
                    handler: (e) => {
                        console.log(e, 'Confirm Ok');
                        this.entity['mark'] = e['mark'];
                        this.entity['model'] = e['model'];
                        this._profile.createCar(this.entity).subscribe(res => {
                            this._profile.get(this.user['id']).subscribe(user => this.user = user['entity']);
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    async usePromo() {
        const alert = await this.alertController.create({
            header: 'Промо код',
            inputs: [
                {
                    name: 'promo',
                    placeholder: 'Код',
                    type: 'text'
                }
            ],
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (e) => {
                        console.log(e, 'Confirm Cancel');
                    }
                }, {
                    text: 'Принять',
                    handler: (e) => {
                        console.log(e, 'Confirm Ok');
                        this._profile.setPromo({ promo: e['promo'] }).subscribe();
                        //    e['promo'];
                    }
                }
            ]
        });

        await alert.present();
    }

    popover(ev) {
        this.presentPopover(ev);
    }

    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: PopoverProfile,
            event: ev,
            translucent: true
        });
        return await popover.present();
    }

    saveAuto(car: object) {
        if (car['last_change'] && car['odometer'] && car['reminder']) {
            this._profile.saveAuto(car).subscribe();
        }
    }

    rmAuto(car: object) {
        this._profile.rmAuto(car).subscribe(res => {
            this._profile.get(this.user['id']).subscribe(user => this.user = user['entity']);
        });
    }


}
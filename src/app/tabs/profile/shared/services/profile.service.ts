import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { Resolve } from "@angular/router";
import { tap } from "rxjs/operators";

@Injectable()
export class ProfileService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController) {
        super(request, toastController);
        this.service_name = 'profile';
    }

    resolve() {
        if (localStorage['userId']) {
            return this.get(localStorage['userId']);
        }
    }


    getUser(userId: number) {
        let url = this.url('index');
        url += '/' + userId;
        return this.request.get(url).pipe(
            tap(() => {

            }, err => {
                this.presentToastWithOptions('danger', 'Пользователь не найден');
            }));
    }
    createCar(data: object) {
        let url = this.url('createCar');
        return this.request.post(url, data).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Автомобиль успешно добавлено');
                }
            })
        );
    }

    saveAuto(data: object) {
        let url = this.url('saveAuto');
        url += '/' + data['id'];
        return this.request.put(url, data).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Данные обновлены');
                }
            })
        );
    }

    rmAuto(data: object) {
        let url = this.url('saveAuto');
        url += '/' + data['id'];
        return this.request.delete(url).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Автомобиль удален');
                }
            })
        );
    }

    update(data: object) {
        let url = this.url('update');
        url += '/' + data['id'];
        return this.request.put(url, data).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Профиль успешно обновлен');
                }
            })
        );
    }
    //     @ngx-progressbar/core": "^5.2.1",
    //     "@ngx-progressbar/http": "^5.2.1",
    //  "ngx-progressbar": "^2.1.1",
    updateNot(data: object) {
        let url = this.url('update');
        url += '/' + data['id'];
        return this.request.put(url, data);
    }

    setPromo(data: object) {
        let url = this.url('promo');
        return this.request.post(url, data).pipe(
            tap(res => {
                if (res['status']) {
                    this.presentToastWithOptions('success', 'Промо код успешно активирован');
                }
            })
        );
    }
}
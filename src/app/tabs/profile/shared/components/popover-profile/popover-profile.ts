import { AuthService } from 'src/app/auth/shared/services/auth.service';
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AlertController, PopoverController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { DataNotService } from 'src/app/shared/providers/data-not-count.service';
import {SocialLoginService} from '../../../../../auth/shared/services/social-login.service';

@Component({
    selector: 'popover-profile',
    templateUrl: './popover-profile.html',
    styleUrls: ['./popover-profile.scss']
})
export class PopoverProfile {

    constructor(public router: Router,
        private auth: AuthService,
        private countNot: DataNotService,
        private alertController: AlertController,
        private socialLoginService: SocialLoginService,
        private popover: PopoverController,
        private storage: Storage) {
    }

    openProfile() {
        this.popover.dismiss();
        this.router.navigate(['/app/tabs/profile/profile']);
    }

    exit() {
        this.popover.dismiss();
        this.presentAlertConfirm();
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            header: 'Внимание!',
            message: 'Вы точно хотите выйти?',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Выход',
                    handler: () => {
                        this.auth.logout().subscribe();
                        this.socialLoginService.logoutGoogle()
                            .finally((res: any) => {
                                console.log(res);
                            });
                        localStorage.clear();
                        this.storage.clear();
                        this.countNot.updateData(0);
                        this.router.navigate(['/app/tabs/oil']);
                        console.log('Confirm Okay');
                    }
                }
            ]
        });
        await alert.present();

    }
}
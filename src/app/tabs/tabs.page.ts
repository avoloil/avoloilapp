import { DataNotService } from './../shared/providers/data-not-count.service';
import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import {IonTabs, NavController} from '@ionic/angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserUpdateService } from '../shared/providers/user-update';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public user: object = {};
  private unreadList: any;
  unreadArr: Array<object> = [];
  private count: number = 0;

  constructor(
    private _userProv: UserUpdateService,
    private countNot: DataNotService,
    private navCtrl: NavController,
    private db: AngularFireDatabase,
    public route: ActivatedRoute) {
    this.countNot.count.subscribe(count => {
      this.count = count;
    });
  }

  openOil() {
    this.navCtrl.navigateRoot(['/app/tabs/oil']);
  }

  ionViewWillEnter() {
      this._userProv.user.subscribe(user => {
      if (this.user !== user) {
        this.user = user;
        if (localStorage['token'] && this.user != null) this.unreadAll();
      }
    });
  }

  async unreadAll() {
    if (localStorage['token']) {
      this.count = 0;
      this.user['chats'].map((el, index) => {
        this.unreadList = this.db.list('chats/' + el);
        this.unreadList.valueChanges()
          .subscribe(data => {
            if (localStorage['token']) {
              if (data.length > 1) {
                if (data[1]) {
                  let roomIndex = this.unreadArr.findIndex(el => el['room_id'] == data[1]['room_id']);
                  if (roomIndex !== -1) {
                    this.unreadArr[roomIndex] = data[1];
                    this.sumUnread();
                  } else {
                    this.unreadArr.push(data[1]);
                    this.sumUnread();
                  }
                }
              }
            }
          });
      });
    }
  }

  async sumUnread() {
    this.count = 0;
    this.unreadArr.map(el => {
      if (el[this.user['id']]) {
        this.count += el[this.user['id']];
      }
    });
  }

}

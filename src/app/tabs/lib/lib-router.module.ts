import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LibComponent } from "./lib.component";
import { LibService } from "./shared/services/lib.service";
import { LibInfoComponent } from "./lib-info/lib-info.component";

const routes: Routes = [
    {
        path: '',
        component: LibComponent,
        resolve: {
            data: LibService
        }
    },
    {
        path: 'info/:id',
        component: LibInfoComponent,
        resolve: { libInfo: LibService }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LibRouterModule { }
import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { fromEvent } from "rxjs";
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
    selector: 'app-tabs-lib-info',
    templateUrl: './lib-info.component.html',
    styleUrls: ['./lib-info.component.scss']
})
export class LibInfoComponent {

    public entity: any;
    public data: any;
    private content: any;
    private status: boolean = true;
    private responce: any;

    constructor(public route: ActivatedRoute, private photoViewer: PhotoViewer) { }

    ionViewWillEnter() {
        this.route.data.forEach(res => {
            this.responce = res['libInfo'];

        });
        setTimeout(() => {
            let target = document.querySelector('#content_div');
            let div = document.createElement('div');
            div.innerHTML = this.responce;
            target.parentNode.insertBefore(div, target);
            target.parentNode.insertBefore(div, target.nextSibling);
            console.log(document.getElementsByTagName('img'));
            let imgCollection = document.getElementsByTagName('img');
            for (let i = 0; i < imgCollection.length; i++) {
                let img = imgCollection[i];
                let scrollBlock = document.createElement('div');
                scrollBlock.innerHTML = img.outerHTML;
                img.parentNode.insertBefore(scrollBlock, img);
                img.parentNode.insertBefore(scrollBlock, img.nextSibling);
                img.parentNode.removeChild(img);
                let stream = fromEvent(scrollBlock, 'touchend');
                stream.subscribe(() => {
                    this.photoViewer.show(img['src']);
                });
            }
            // let img = document.getElementsByTagName('img')[0];
        });
    }

    back() {
        window.history.back();
    }

}
import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { ToastController } from "@ionic/angular";
import { switchMap, tap } from "rxjs/operators";
import * as queryString from 'query-string';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class LibService extends EntityService implements Resolve<any> {

    constructor(public request: RequestService, public toastController: ToastController, public http: HttpClient,
    ) {
        super(request, toastController);
        this.service_name = 'lib';
    }

    resolve(route: ActivatedRouteSnapshot) {
        let id = route.params['id'];
        if (id) {
            return this.getLibsOnce(id).pipe(switchMap(test => this.getFileData({ path: test['entity']['link'] })));
        } else {
            return this.getLibs();
        }
    }

    getLibs() {
        let url = this.url('index');
        return this.request.get(url);
    }

    getLibsOnce(id: number) {
        let url = this.url('index');
        url += '/' + id;
        return this.request.get(url);
    }

    getFileData(data: object) {
        let url = this.url('file');
        return this.getFile(url, data);
    }

    /**
    *
    * @param {string} url
    * @param {Object} body
    * @returns {Observable<Object>}
    */
    public getFile(url: string, body: Object = null) {
        // this.ngProgress.start();
        if (body !== null) {
            if (Object.keys(body).length > 0) {
                url += '?' + queryString.stringify(body);
            }
        }
        return this.http.get(url, { responseType: 'text' })
            .pipe(tap(() => {
                // this.ngProgress.done();
            }));
    }
}
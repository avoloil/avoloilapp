import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "src/app/shared/shared.module";
import { LibComponent } from "./lib.component";
import { LibRouterModule } from "./lib-router.module";
import { LibService } from "./shared/services/lib.service";
import { LibInfoComponent } from "./lib-info/lib-info.component";
import { PhotoViewer } from "@ionic-native/photo-viewer/ngx";

@NgModule({
    imports: [
        IonicModule,
        SharedModule,
        LibRouterModule,
    ],
    declarations: [
        LibComponent,
        LibInfoComponent,
    ],
    providers: [
        LibService,
        PhotoViewer
    ]
})
export class LibModule { }
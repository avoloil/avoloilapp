import { Component } from "@angular/core";
import { InfinitiScroll } from "src/app/shared/components/infiniti-scrolle.component";
import { LibService } from "./shared/services/lib.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: 'app-tabs-lib',
    templateUrl: './lib.component.html',
    styleUrls: ['./lib.component.scss']
})
export class LibComponent extends InfinitiScroll {

    constructor(
        public libService: LibService,
        public route: ActivatedRoute,
        public router: Router
    ) {
        super(libService);
    }

    ionViewWillEnter() {
        this.route.data.forEach(res => {
            this.toItems = res['data']['entity']['to'];
            this.list = res['data']['entity']['data'];
            this.params['page'] = res['data']['entity']['current_page'];
            this.totalItems = res['data']['entity']['total'];
        });        // this.route.data.forEach(data => {
        //     this.list = data['lib']['entity']['data'];
        // });
    }

    openInfo(id: number) {
        this.router.navigate(['/app/tabs/lib/info', id]);
    }

}
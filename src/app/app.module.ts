import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutologinGuard } from './shared/guard/autologin-guard.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { ProfileService } from './tabs/profile/shared/services/profile.service';
import { DataNotService } from './shared/providers/data-not-count.service';
import { UserUpdateService } from './shared/providers/user-update';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
    SharedModule,
    BrowserAnimationsModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    FCM,
    StatusBar,
    SplashScreen,
    DataNotService,
    AutologinGuard,
    ProfileService,
    UserUpdateService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

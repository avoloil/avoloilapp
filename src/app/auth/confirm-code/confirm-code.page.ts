import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

declare interface UserData {
    email: string;
    code: string;
}

@Component({
    selector: 'app-confirm',
    templateUrl: './confirm-code.page.html',
    styleUrls: ['../auth.page.scss', './confirm-code.page.scss']
})
export class ConfirmPage implements OnInit {

    public confirm: any;

    constructor(
        private formBuilder: FormBuilder,
        public auth: AuthService,
        public router: Router) {
        this.confirm = this.formBuilder.group({
            code: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        // console.log(this.route.queryParams['_value'].email);
        // this.userData.email = this.route.queryParams['_value'].email;
        // console.log('Sign Up component is loaded');
    }

    confirmCodeForm() {
        this.auth.confirm(this.confirm.value).subscribe();
    }

    back() {
        window.history.back();
    }

    openRegister() {
        this.router.navigate(['auth/register']);
    }

    openLogin() {
        this.router.navigate(['auth/login']);
    }

}

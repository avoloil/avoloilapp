import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

declare interface UserData {
    code: string;
    password: string;
    passwordConfirm: string;
}

@Component({
    selector: 'app-change',
    templateUrl: './change.page.html',
    styleUrls: ['../auth.page.scss', './change.page.scss']
})
export class ChangePage implements OnInit {

    public change: any;

    constructor(
        private formBuilder: FormBuilder,
        public route: ActivatedRoute,
        public auth: AuthService,
        public router: Router
    ) {
        this.change = this.formBuilder.group({
            code: [route.queryParams['_value']['code'], [Validators.required]],
            password: ['', [Validators.required]],
            passwordConfirm: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        console.log('Sign Up component is loaded');
    }

    changeForm() {
        this.auth.change(this.change.value).subscribe();
    }

    back() {
        window.history.back();
    }

    openRegister() {
        this.router.navigate(['auth/register']);
    }

    openLogin() {
        this.router.navigate(['auth/login']);
    }

}

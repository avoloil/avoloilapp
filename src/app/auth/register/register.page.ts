import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from '../shared/services/auth.service';
import { ModalController } from '@ionic/angular';
import { ModalSelecteCity } from 'src/app/shared/components/modal-selecte-city/modal-selecte-city';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['../auth.page.scss', './register.page.scss']
})
export class RegisterPage {

    // @ViewChild('input') private input: any;

    public reg: FormGroup;
    // public roles: any;
    // customAlertOptions: any = {
    //     header: 'Роль',
    //     subHeader: 'Выберите роль поopenModalльзователя',
    //     translucent: true
    // };

    customHeader: any = {
        header: 'Страна'
    };

    private pwdPattern = '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}';
    emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

    constructor(
        private formBuilder: FormBuilder,
        public router: Router,
        public auth: AuthService,
        private modalController: ModalController
    ) {
        this.reg = this.formBuilder.group({
            username: new FormControl('', [Validators.required]),
            lastname: new FormControl('', [Validators.required]),
            city: new FormControl(''),
            country_id: new FormControl(null, [Validators.required]),
            mobile: new FormControl(''),
            email: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
            passwordConfirm: new FormControl('', [Validators.required]),
            role: new FormControl('USER'),
            lat: new FormControl(null),
            lng: new FormControl(null)
        });
    }

    async ionViewWillEnter() {
        // this.input.setFocus();
        console.log(1);

    }

    regForm() {
        this.reg.value.email.toLowerCase();
        this.auth.signUp(this.reg.value).subscribe(res => {
            if (res['success']) {
                this.router.navigate(['/auth']);
            }
        });
    }

    back() {
        this.router.navigate(['auth/login']);
    }

    openRegister() {
        this.router.navigate(['auth/register']);
    }

    forgot() {
        this.router.navigate(['auth/forgot-password']);
    }

    openModal() {
        this.presentModal();
    }

    async presentModal() {
        const modal = await this.modalController.create({
            component: ModalSelecteCity
        });
        modal.onDidDismiss().then(res => {
            this.reg.patchValue({ city: res['data']['city'], lat: res['data']['latitude'], lng: res['data']['longitude'] });
            switch (res['data']['country']) {
                case 'Беларусь': {
                    this.reg.patchValue({ country_id: 2 });
                    break;
                }
                case 'Россия': {
                    this.reg.patchValue({ country_id: 1 });
                    break;
                }
                case 'Казахстан': {
                    this.reg.patchValue({ country_id: 3 });
                    break;
                }
            }
        });
        return await modal.present();
    }
}

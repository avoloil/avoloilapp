import { Injectable } from "@angular/core";
import { EntityService } from "src/app/shared/base/entity.service";
import { RequestService } from "src/app/shared/services/request.service";
import { ToastController } from "@ionic/angular";
import { Router } from "@angular/router";
import { tap } from "rxjs/operators";


@Injectable()
export class AuthService extends EntityService {

    constructor(public request: RequestService, public toastController: ToastController, public router: Router) {
        super(request, toastController);
        this.service_name = 'auth';
    }

    signIn(data: any) {
        let url = this.url('login');
        return this.request.post(url, data)
            .pipe(tap(res => {
                if (res['success']) {
                    localStorage['login'] = data['login'];
                } else {
                    this.presentToastWithOptions('danger', res['error']);
                }
            }));
    }

    signUp(data: any) {
        let url = this.url('register');
        return this.request.post(url, data)
            .pipe(tap(res => {
                if (res['success']) {
                    const msg = this.msg('register');
                    this.presentToastWithOptions('success', 'Вы зарегистрированы. Выполните вход');
                    this.router.navigate(['auth']);
                } else {
                    let msg = '';
                    for (let i in res['error']) {
                        msg = res['error'][i];
                    }
                    this.presentToastWithOptions('danger', msg);
                }
            }));
    }

    forgot(credentials) {
        const url = this.url('forgot');
        return this.request.post(url, credentials)
            .pipe(tap(res => {
                if (res['status']) {
                    this.router.navigate(['auth/confirm'], { queryParams: { email: credentials['email'] } });
                } else {
                    this.presentToastWithOptions('danger', res['error']);
                }
            }, err => {
                const msg = this.msg('forgot_error');
                this.presentToastWithOptions('danger', err);
            }));
    }

    confirm(credentials) {
        const url = this.url('confirm');
        return this.request.post(url, credentials)
            .pipe(tap(res => {
                if (res['status']) {

                    this.router.navigate(['auth/change'], { queryParams: { code: credentials['code'] } });
                } else {
                    this.presentToastWithOptions('danger', res['error']);
                }

            }));
    }

    verif(credentials) {
        const url = this.url('varif');
        return this.request.post(url, credentials)
            .pipe(tap(res => {
                if (res['status']) {
                    this.router.navigate(['auth'], { queryParams: { verification_code: credentials['verification_code'] } });
                } else {
                    this.presentToastWithOptions('danger', res['error']);
                }

            }));
    }

    change(credentials) {
        const url = this.url('change');
        if (credentials['password'] === credentials['passwordConfirm']) {
            return this.request.post(url, credentials)
                .pipe(tap(res => {
                    if (res['status']) {
                        this.router.navigate(['auth/login']);
                    } else {
                        this.presentToastWithOptions('danger', res['error']);
                    }
                }, err => {
                    this.presentToastWithOptions('danger', err);

                }));
        } else {
            const msg = this.msg('change_error');
            this.presentToastWithOptions('danger', msg);
        }
    }

    getRole() {
        let url = this.url('role');
        return this.request.get(url);
    }

    sendPushNot(data) {
        let url = this.url('save');
        return this.request.post(url, data);
    }

    saveToken(data: object) {
        let url = this.url('save');
        return this.request.post(url, data);
    }

    logout() {
        let url = this.url('logout');
        return this.request.post(url, { tokenDevice: localStorage['tokenDevice'] });
    }
}
import {Injectable} from '@angular/core';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {EntityService} from '../../../shared/base/entity.service';
import {Observable} from 'rxjs';
import {RequestService} from '../../../shared/services/request.service';
import {ToastController} from '@ionic/angular';

@Injectable()
export class SocialLoginService extends EntityService {

    constructor(private facebook: Facebook,
                private google: GooglePlus,
                public request: RequestService,
                public toast: ToastController) {
        super(request, toast);
        this.service_name = 'auth';
    }

    async loginViaFacebook(): Promise<FacebookLoginResponse> {
        return await this.facebook.login(['public_profile']);
    }

    getFacebookUserData(data: string): Promise<any> | any {
        return this.facebook.api(data, ['email']);
    }

    loginViaGoogle(): Promise<any> | any {
        return this.google.login({
            webClientId: '488498311058-dl47vo1si3gvne31lhfpnft1pf97frd8.apps.googleusercontent.com',
            offline: true
        });
    }

    logoutGoogle(): Promise<any> | any {
        return this.google.logout();
    }

    socialLogin(data: any): Observable<any> | Promise<any> | any {
        const url = this.url('socialLogin');
        return this.request.post(url, data);
    }
}

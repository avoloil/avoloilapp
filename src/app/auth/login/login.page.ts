import {Component} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../shared/services/auth.service';
import {Storage} from '@ionic/storage';
import {FCM} from '@ionic-native/fcm/ngx';
import {SocialLoginService} from '../shared/services/social-login.service';
import {FacebookLoginResponse} from '@ionic-native/facebook';

@Component({
    selector: 'app-login',
    templateUrl: 'login.page.html',
    styleUrls: ['../auth.page.scss', 'login.page.scss']
})
export class LoginPage {

    public log: any;
    public login: boolean = false;

    private pwdPattern = '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}';
    // unamePattern = '^[a-z0-9_-]{8,15}$';
    emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

    constructor(private formBuilder: FormBuilder,
                public router: Router,
                public auth: AuthService,
                private fcm: FCM,
                public storage: Storage,
                private socialLoginService: SocialLoginService
    ) {
        this.log = this.formBuilder.group({
            email: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
            login: new FormControl(localStorage['login'])
        });
    }

    ionViewWillEnter() {
        if (localStorage['tokenDevice']) {
            this.auth.logout().subscribe();
        }
    }

    async logForm() {
        this.log.value.email.toLowerCase();
        // const login = await this.storage.set('user', this.log.value);
        this.auth.signIn(this.log.value).subscribe(res => {
            if (res['success']) {
                localStorage['token'] = res['data']['token'];
                if (res['data']['user']['partner']) {
                    localStorage['partnerId'] = res['data']['user']['partner']['id'];
                }
                localStorage['latitude'] = res['data']['user']['lat'];
                localStorage['longitude'] = res['data']['user']['lng'];
                localStorage['user'] = res['data']['user'];
                localStorage['userId'] = res['data']['user']['id'];
                localStorage['country'] = res['data']['user']['country'];
                localStorage['city'] = res['data']['user']['city'];
                this.storage.set('user', res['data']['user']);
                this.router.navigate(['/app/tabs/profile']);
                if (!localStorage['tokenDevice']) {
                    this.fcm.getToken().then(token => {
                        console.log(token);
                        localStorage['tokenDevice'] = token;
                        this.auth.saveToken({token: localStorage['tokenDevice']}).subscribe();
                    });
                } else {
                    this.auth.saveToken({token: localStorage['tokenDevice']}).subscribe();
                }
            }
        });
    }

    openRegister() {
        this.router.navigateByUrl('auth/register');
    }

    autologin(ev) {
        this.login = ev.detail.checked;
    }

    back() {
        this.router.navigate(['/app/tabs']);
    }

    forgot() {
        this.router.navigate(['auth/forgot-password']);
    }


    async loginFacebook() {
        const res = await this.socialLoginService.loginViaFacebook();
        console.log(res);
        const result = await this.socialLoginService.getFacebookUserData('me?fields=id,first_name,last_name,address,email');
        console.log(result);
        const obj = {
            email: result.email,
            username: result.first_name,
            lastname: result.last_name,
            social_id: res.authResponse.userID
        };
        if (obj.email) {
            this.socLogin(obj);
        } else {
            this.auth.presentToastWithOptions(
                'danger',
                'К вашей учётной записи не привязан email. Пожалуйста, добавьте email на странице Facebook',
                true
            );
        }
    }

    async loginGoogle() {
        await this.socialLoginService.loginViaGoogle()
            .then((res: any) => {
                console.log(res);
                const obj = {
                    email: res.email,
                    username: res.givenName,
                    lastname: res.familyName,
                    social_id: res.userId
                };
                this.socLogin(obj);
            }).catch((err: any) => {
                console.log(err);
            });
    }

    socLogin(data: any) {
        this.socialLoginService.socialLogin(data)
            .subscribe((response: any) => {
                if (response.success) {
                    localStorage.setItem('token', response.token);
                    localStorage.setItem('user', JSON.stringify(response.data));
                    this.router.navigateByUrl('app/tabs/profile', {replaceUrl: true});
                }
                console.log(response);
            });
    }
}

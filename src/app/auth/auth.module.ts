import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { AuthPageRoutingModule } from "./auth-routing.module";
import { AuthPage } from "./auth.page";
import { AuthService } from './shared/services/auth.service';
import { RegisterPage } from './register/register.page';
import { LoginPage } from './login/login.page';
import { ForgotPasswordPage } from './forgot-password/forgot-password.page';
import { ConfirmPage } from './confirm-code/confirm-code.page';
import { ChangePage } from './change/change.page';
import { SharedModule } from '../shared/shared.module';
import { ModalSelecteCity } from '../shared/components/modal-selecte-city/modal-selecte-city';
import { VerificationComponent } from './verification/verification';
import { FCM } from '@ionic-native/fcm/ngx';
import {SocialLoginService} from './shared/services/social-login.service';
import {Facebook} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';

@NgModule({
    imports: [
        IonicModule,
        // CommonModule,
        // FormsModule,
        // ReactiveFormsModule,
        SharedModule,
        AuthPageRoutingModule,
        // LoginPageModule,
        // RegisterPageModule
    ],
    declarations: [
        AuthPage,
        LoginPage,
        RegisterPage,
        ForgotPasswordPage,
        ConfirmPage,
        ChangePage,
        VerificationComponent
        // ModalSelecteCity
    ],
    providers: [AuthService, FCM, SocialLoginService, Facebook, GooglePlus],
    // entryComponents: [ModalSelecteCity]
})
export class AuthModule { }

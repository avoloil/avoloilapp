import { Component, NgZone } from "@angular/core";
import { ModalController } from "@ionic/angular";

@Component({
    selector: 'modal-selecte-city',
    templateUrl: './modal-selecte-city.html',
    styleUrls: ['./modal-selecte-city.scss']
})
export class ModalSelecteCity {
    autocompleteItems;
    autocomplete;

    latitude: number = 0;
    longitude: number = 0;
    geo: any;
    city: any;
    country: any;
    service = new google.maps.places.AutocompleteService();

    constructor(private zone: NgZone, private modalController: ModalController) {
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    }

    dismiss() {
        this.modalController.dismiss();
    }

    chooseItem(item: any) {
        console.log(item);

        this.geo = item;
        this.geoCode(this.geo);//convert Address to lat and long
    }

    updateSearch() {

        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }

        let me = this;
        this.service.getPlacePredictions({

            input: this.autocomplete.query,
            componentRestrictions: {
                country: ['ru', 'kz', 'by'],
            },
            types: ['(cities)']
        }, (predictions, status) => {
            me.autocompleteItems = [];
            me.zone.run(() => {
                if (predictions != null) {
                    predictions.forEach((prediction) => {
                        me.autocompleteItems.push(prediction.description);
                    });
                }
            });
        });
    }

    //convert Address string to lat and long
    geoCode(address: any) {
        console.log(address);

        let geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, (results, status) => {
            console.log(results);
            this.city = results[0]['address_components'][0]['long_name'];
            results[0]['address_components'].map(el => {
                if (el['types'][0] === 'country') {
                    this.country = el['long_name'];
                } else if (el['types'][0] === 'locality') {
                    this.city = el['long_name'];
                }
            });
            this.latitude = results[0].geometry.location.lat();
            this.longitude = results[0].geometry.location.lng();
            this.modalController.dismiss({ city: this.city, country: this.country, latitude: this.latitude, longitude: this.longitude });
            console.log("lat: " + this.latitude + ", long: " + this.longitude);
        });
    }
}
import { Injectable } from "@angular/core";
import { EntityService } from "../base/entity.service";

@Injectable()
export class InfinitiScroll {

    public list: any[] = [];
    public id: number | string;
    public oil_id: number | string;
    public pageUrl: boolean = false;
    public favorite: boolean = false;
    public is_filter: boolean = false;
    public totalItems: number;
    public addSale: boolean = false;
    public currentPage: number;
    public itemsPerPage: number;
    public userSell = {
        canisters: [],
        user_id: null,
    };
    public toItems: number;
    public view: boolean = false;
    public params: object = {};
    public canisters: Array<number> = [];
    public assort: boolean = false;
    constructor(public service: EntityService) {
        this.currentPage = 1;
        this.itemsPerPage = 10;
        this.params = { page: this.currentPage, limit: this.itemsPerPage };
    }

    getAll() {
        if (this.pageUrl) {
            this.id = localStorage['partnerId'] + '/getCanisters';
        }
        if (this.is_filter) {
            this.id = this.oil_id;
            this.params = {};
        }
        this.service.get(this.id, this.params)
            .subscribe(success => {
                if (success['entity']) {
                    this.list = this.list.concat(success['entity']['data']);
                    if (this.favorite) {
                        this.list.map(el => {
                            if (el['favorite'] == null) {
                                el['favorite'] = true;
                            }
                        });
                    }
                    if (this.assort) {
                        this.list.map(el => {
                            if (el['in_assortment']) {
                                this.canisters.push(el['id']);
                            }
                        });
                    }
                    if (this.addSale) {
                        this.list.map(el => {
                            if (!el['count']) el['count'] = 0
                        });
                    }
                    this.params['page'] = success['entity']['current_page'];
                    this.totalItems = success['entity']['total'];
                    this.toItems = success['entity']['to'];
                    this.view = true;
                } else {
                    this.list = [];
                    success['data']['result'].forEach(item => {
                        this.list.push(item.oil);
                    });
                }
            });
    }

    loadData(event) {
        setTimeout(() => {
            this.params['page']++;
            if (this.toItems == this.totalItems) {
                this.params['page']--;
            } else {
                this.getAll();
            }
            event.target.complete();
        }, 500);
    }

    doRefresh(event) {
        setTimeout(() => {
            if (this.addSale) {
                this.userSell.canisters = [];
            }
            this.list = [];
            this.params['page'] = 1;
            this.getAll();
            event.target.complete();
        }, 1000);
    }
}
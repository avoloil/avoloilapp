import { Directive, Input, Output } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import * as queryString from 'query-string';
import { Action } from "rxjs/internal/scheduler/Action";

@Directive({
    selector: '[unreadMessages]',
    exportAs: 'unread'
})
export class UnreadMessagesDirective {

    @Input('room_id') private room_id: number;
    @Output('unread') unread: number;
    private user: object = {};
    unreadList$: any;
    messageList$: any;

    constructor(private db: AngularFireDatabase) {

    }

    ngAfterViewInit() {
        this.unreadList$ = this.db.object('chats/' + this.room_id + '/unread');
        this.unreadList$.snapshotChanges().subscribe(action => {
            if (action.payload.val() != null) {
                this.unread = action.payload.val()[localStorage['userId']];
                console.log(action.payload.val());
            }
        });
    }
}
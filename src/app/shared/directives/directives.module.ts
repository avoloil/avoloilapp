import { UnreadMessagesDirective } from './unread-messages';
import { NgModule } from '@angular/core';
import { LastMessageMessagesDirective } from './last-message.directive';

@NgModule({
    declarations: [
        UnreadMessagesDirective, LastMessageMessagesDirective],
    exports: [
        UnreadMessagesDirective, LastMessageMessagesDirective]
})

export class DirectiveModule { }
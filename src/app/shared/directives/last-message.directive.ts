import { Directive, Input, Output } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import * as queryString from 'query-string';

@Directive({
    selector: '[lastMessages]',
    exportAs: 'lastMessage'
})
export class LastMessageMessagesDirective {

    @Input('room_id') private room_id: number;
    @Output('lastMessage') lastMessage: number;
    @Output('chat_user_id') private chat_user_id: number;
    @Output('unread') private unread: number;
    private user: object = {};
    messageList$: any;

    constructor(private db: AngularFireDatabase) {

    }

    ngAfterViewInit() {
        this.messageList$ = this.db.list('chats/' + this.room_id + '/messages', ref => ref.limitToLast(1));
        this.messageList$.valueChanges().subscribe(action => {
            // console.log(action);
            if (action.length) {
                this.lastMessage = action[0]['text'];
                this.chat_user_id = action[0]['chat_user_id'];
                this.unread = action[0]['unread'];
            }
        });
    }
}
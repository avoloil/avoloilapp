import {Injectable} from '@angular/core';
import {Request} from '../interfaces/request.interface';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/map';
import * as queryString from 'query-string';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';
import {ToastController} from '@ionic/angular';

// import { NgProgress } from 'ngx-progressbar';

@Injectable()
export class RequestService implements Request {

    private urlToRedirect: string;
    private config = {};

    constructor(public http: HttpClient,
                private router: Router,
                public toastController: ToastController) {
        this.urlToRedirect = 'auth';
    }

    errorHandler(error: HttpErrorResponse) {
        console.log(error);
        switch (error.status) {
            case 401:
                localStorage.clear();
                this.router.navigateByUrl(this.urlToRedirect);
                break;
            default: {
                this.presentToastWithOptions('danger', error);
            }
        }
    }

    /**
     *
     * @param {string} url
     * @param {Object} body
     * @returns {Observable<Object>}
     */
    public get(url: string, body: Object = null) {
        // this.ngProgress.start();
        if (body !== null) {
            if (Object.keys(body).length > 0) {
                url += '?' + queryString.stringify(body);
            }
        }
        return this.http.get(url)
            .pipe(tap(() => {
                // this.ngProgress.done();

            }, error => {
                this.errorHandler(error);
            }));
    }

    /**
     *
     * @param url
     * @param credentials
     * @returns {Observable<ArrayBuffer>}
     */
    public post(url: string, credentials: any) {
        // this.ngProgress.start();

        return this.http.post(url, credentials)
            .pipe(tap(() => {
                // this.ngProgress.done();

            }, error => {
                this.errorHandler(error);
            }));
    }

    /**
     *
     * @param {string} url
     * @param credentials
     * @returns {Observable<Object>}
     */
    public put(url: string, credentials: any) {
        // this.ngProgress.start();

        return this.http.put(url, credentials)
            .pipe(tap(() => {
                // this.ngProgress.done();

            }, error => {
                this.errorHandler(error);
            }));

    }

    /**
     *
     * @param {string} url
     * @returns {Observable<Object>}
     */
    public delete(url: string) {
        // this.ngProgress.start();

        return this.http.delete(url)
            .pipe(tap(() => {
                // this.ngProgress.done();

            }, error => {
                this.errorHandler(error);
            }));
    }

    presentToastWithOptions(type: any, data: any) {
        this.config['type'] = type;
        if (data instanceof HttpErrorResponse) {
            if (data['error'] instanceof Object && data['error']['message'] == null && typeof data['error']['message'] !== 'undefined') {
                if (data['error']['message'].length >= 0) {
                    for (let item in data['error']) {
                        this.config['life'] = 0;
                        this.toast(data['error'][item][0], 'Ок', this.config);
                    }
                }

            }
            if (data['error'] instanceof Object) {
                for (let item in data['error']) {
                    this.config['life'] = 0;
                    this.toast(data['error'][item][0], 'Ок', this.config);
                }
            }
            if (typeof data['error']['message'] === 'string' && typeof data['error']['message'] !== 'undefined') {
                if (data['error']['message'].length > 0) {
                    this.config['life'] = 0;
                    this.toast(data['error']['message'], 'Ок', this.config);
                }

            }
            if (data['error']['message'] != null && typeof data['error']['message'] !== 'undefined') {
                if (!data['error']['message'].length) {
                    this.toast('Ошибка на стороне сервера', 'Ok', this.config);
                }
            }
            if (data.status === 401) {
                this.router.navigate(['auth']);
            }
        } else {
            this.config['life'] = 2000;
            this.toast(data, null, this.config);
        }
    }

    async toast(message, button, config) {
        const toast = await this.toastController.create({
            message,
            showCloseButton: !config['life'],
            position: 'top',
            duration: config['life'],
            color: config['type'],
            cssClass: 'normalToast',
            closeButtonText: button
        });
        toast.present();
    }
}

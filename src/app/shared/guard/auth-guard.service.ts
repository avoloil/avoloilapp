import { Injectable } from "@angular/core";
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from "@angular/router";
import { Observable } from "rxjs";
import { AlertController } from "@ionic/angular";

@Injectable()
export class AuthGuard implements CanActivate {

    public token: string;

    constructor(
        public alertController: AlertController,
        private router: Router) {
    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (localStorage['token']) {
            return true;
        } else {
            this.presentAlertConfirm();
            return false;
        }
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            header: 'Внимание!',
            message: 'Что бы открыть доступ авторизируйтесь.',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Вход',
                    handler: () => {
                        this.router.navigate(['/auth']);
                        console.log('Confirm Okay');
                    }
                }
            ]
        });
        await alert.present();

    }
}


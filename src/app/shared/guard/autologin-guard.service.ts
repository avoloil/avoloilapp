import {Injectable} from '@angular/core';
import {Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable()
export class AutologinGuard implements CanActivate {

    public autologin: boolean;

    constructor(
        private router: Router) {
        this.autologin = (localStorage['login'] === 'true');
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.autologin) {
            this.router.navigate(['/app/tabs/']);
            return false;
        } else {
            return true;
        }
    }
}
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class UserUpdateService {

    public user: BehaviorSubject<any> = new BehaviorSubject(null);

    constructor(private storage: Storage) { }

    load(): void {
        // let data = await this.storage.get('count');
        this.storage.get('user').then((data) => {
            this.user.next(data);
        });
    }

    updateData(data): void {
        this.storage.set('user', data);
        this.user.next(data);
    }

}
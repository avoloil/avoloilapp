import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable({
    providedIn: 'root'
})
export class DataNotService {

    public count: BehaviorSubject<any> = new BehaviorSubject(0);

    constructor(private storage: Storage) { }

    load(): void {
        // let data = await this.storage.get('count');
        this.storage.get('count').then((data) => {
            this.count.next(data);
        });
    }

    updateData(data): void {
        this.storage.set('count', data);
        this.count.next(data);
    }

}
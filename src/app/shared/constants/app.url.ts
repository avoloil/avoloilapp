import { environment } from "../../../environments/environment";

const api = environment.apiHost;

export const APP_URL = {
    'auth': {
        login: api + 'login',
        register: api + 'register',
        logout: api + 'logout',
        forgot: api + 'user/forgot',
        confirm: api + 'user/confirm',
        varif: api + 'verify',
        change: api + 'user/change',
        role: api + 'role',
        save: api + 'set-device',
        socialLogin: api + 'social-login'
    },
    'lib': {
        index: api + 'study-material',
        file: api + 'getFileData'
    },
    'home': {
        index: api + 'news'
    },
    'oil': {
        getMark: api + 'new-cars',
        getYears: api + 'condition1/',
        getEngineType: api + 'new-cars/',
        getFilter: api + 'condition2/',
        getOilConsumption: api + 'condition3/',
        getResult: api + 'condition4/'
    },
    'oil-list': {
        index: api + 'oil',
        favorite: api + 'user/wishlist'
    },
    'favorite': {
        index: api + 'user/wishlist'
    },
    'oil-info': {
        index: api + 'oil',
        addWish: api + 'user/addWish',
        minusWish: api + 'user/rmWish',
        info: api + 'oilInfo'
    },
    'history': {
        index: api + 'user/getSaleHistory'
    },
    'profile': {
        index: api + 'user',
        update: api + 'user',
        create: api + 'user',
        delete: api + 'user',
        createCar: api + 'car',
        saveAuto: api + 'car',
        promo: api + 'user/setPromocode'
    },
    'shopFavorite': {
        index: api + 'user/favoritePartners'
    },
    'form-gift': {
        setForm: api + 'setForm'
    },
    'sale': {
        index: api + 'partner/sales'
    },
    'shop': {
        index: api + 'partner',
        addFavorite: api + 'user/addToFavorite',
        createChat: api + 'send-shop',
        rmFavorite: api + 'user/rmToFavorite'
    },
    'add-sale': {
        index: api + 'partner'
    },
    'assorts': {
        index: api + 'canister',
        save: api + 'partner',
        canisters: api + 'canister',
        sale: api + 'partner/sell/oil/canisters'
    },
    'canister': {
        index: api + 'canister'
    },
    'merchant': {
        index: api + 'merchant'
    },
    'partner': {
        index: api + 'partner',
        select: api + 'merchant'
    },
    'map': {
        index: api + 'maps/partners',
        add: api + 'user/addToFavorite'
    },
    'chat': {
        index: api + 'chat-list',
        push: api + 'send-message'
    }
};

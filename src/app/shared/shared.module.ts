import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestService } from './services/request.service';
import { EntityService } from './base/entity.service';
import { ConstantHelperService } from './base/constant-helper.service';
import { AuthenticationInterceptor } from './services/authentication.interceptor';
import { IonicStorageModule } from '@ionic/storage';
import { ModalSelecteCity } from './components/modal-selecte-city/modal-selecte-city';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';

@NgModule({
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        HttpClientModule,
        IonicStorageModule.forRoot(),
        NgProgressModule.forRoot({
            spinnerPosition: 'left',
            color: 'darkcyan',
            thick: false,
            spinner: false,
            speed: 200,
            min: 0.15,
            max: 1
        }),
        NgProgressHttpModule.forRoot()
    ],
    providers: [
        RequestService,
        EntityService,
        ConstantHelperService,
        { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true }],
    declarations: [
        ModalSelecteCity
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        CommonModule,
        HttpClientModule,
        ModalSelecteCity,
        NgProgressModule,
        NgProgressHttpModule
    ],
    entryComponents: [ModalSelecteCity]
})
export class SharedModule {
}

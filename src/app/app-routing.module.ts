import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AutologinGuard } from './shared/guard/autologin-guard.service';

const routes: Routes = [
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule', canActivate: [AutologinGuard] },
  { path: 'app', loadChildren: './tabs/tabs.module#TabsModule' },
  { path: '', redirectTo: 'app', pathMatch: 'full' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
